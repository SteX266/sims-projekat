package services;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import classes.Employee;
import classes.Member;
import classes.User;

public abstract class UserService extends Service{

	public UserService() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public boolean changePassword(User user, String password, String oldPassword) throws JsonParseException, JsonMappingException, IOException {
		
		ArrayList<Member> members = repo.getMr().read();
		ArrayList<Employee> employees = repo.getEr().read();
		
		
		String className = user.getClass().getName();
		if (className.equals("classes.Member")) {
			for (Member member : members) {
				if (user.getId() == member.getId()) {
					System.out.println(member.getPassword());
					System.out.println(oldPassword);
					if (member.getPassword().equals(oldPassword)) {
						member.setPassword(password);
						repo.getMr().write(members);
						return true;
					}
					else {
						return false;
					}
					
				}
			}
		}
		else {
			for (Employee employee:employees) {
				if (user.getId() == employee.getId()) {
					if (employee.getPassword().equals(oldPassword)) {
						employee.setPassword(password);
						repo.getEr().write(employees);
						return true;
					}
					else {
						return false;
					}
					
				}
			}
		}
		return false;
		
	
	
		
		
		
	
	}

}
