package services;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.JsonMappingException;

import classes.Book;
import classes.BookCopy;
import classes.BookStatus;
import classes.Borrowing;
import classes.Member;
import classes.ReservationRequest;

public class LibrarianService extends UserService{

	public LibrarianService() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public void approveReservationRequest(ReservationRequest request){
		ArrayList<ReservationRequest> requests = readReservationRequests();
		for (ReservationRequest reservationRequest : requests) {
			if (request.getId() == reservationRequest.getId()) {
				reservationRequest.setIsApproved(true);
				reservationRequest.setApprovalDate(LocalDate.now());
				break;
			}
			
		}
		request.getBookCopy().setStatus(BookStatus.RESERVED);
		writeReservationRequests(requests);
	}
	public void deleteReservationRequest(ReservationRequest request) {
		ArrayList<ReservationRequest> requests = readReservationRequests();
		for (ReservationRequest reservationRequest : requests) {
			if (request.getId() == reservationRequest.getId()) {
				requests.remove(reservationRequest);
				break;
			}
		}
		writeReservationRequests(requests);
		
	}
	public BookCopy findAvailableBookCopy(Book book) {
		ArrayList<BookCopy> bookCopies = readBookCopies();
		for (BookCopy bookCopy : bookCopies) {
			if (book.getId() == bookCopy.getBook().getId()) {
				return bookCopy;
			}
			
		}
		
		return null;
	}
	public void createBorrowing(Member member, BookCopy bookCopy){
		ArrayList<Borrowing> borrowings = readBorrowings();
		Borrowing lastBorrowing = borrowings.stream().collect(Collectors.maxBy(Comparator.comparingInt(Borrowing::getId))).get();
		int id = lastBorrowing.getId() + 1;
		LocalDate returnDate = createReturnDate(member);
		Borrowing newBorrowing = new Borrowing(id, LocalDate.now(),returnDate,bookCopy,member);
		borrowings.add(newBorrowing);
		writeBorrowings(borrowings);
		
		bookCopy.setStatus(BookStatus.BORROWED);
		
		ArrayList<Member> members = readMembers();
		for (Member member2 : members) {
			if (member.getId() == member2.getId()) {
				member2.addBorrowing(newBorrowing);
				break;
			}
			
		}
		writeMembers(members);
	}
	private LocalDate createReturnDate(Member member) {
		LocalDate date = LocalDate.now().plusDays(30);
		
		return date;
	}

}
