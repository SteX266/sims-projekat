package services;

import java.io.IOException;
import java.util.ArrayList;

import classes.Book;
import classes.BookCopy;
import classes.Employee;
import classes.Member;

public class AdministratorService extends UserService {

	public AdministratorService() {
		super();
	}

	public boolean addBook(Book book) {
		boolean succesful = false;
		try {
			ArrayList<Book> books = repo.getBr().read();
			books.add(book);
			repo.getBr().write(books);
			succesful = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return succesful;
	}

	public boolean deleteBook(Book book) {
		boolean succesful = false;
		ArrayList<Book> books;
		try {
			books = repo.getBr().read();
			for (Book b : books) {
				if (book.getId() == b.getId()) {
					books.remove(b);
					succesful = true;
				}
			}
			ArrayList<BookCopy> copies = repo.getBcr().read();
			for (BookCopy bookCopy : copies) {
				if (bookCopy.getBook().getId() == book.getId()) {
					copies.remove(bookCopy);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return succesful;
	}

	public boolean addEmployee(Employee emp) {
		boolean succesful = false;
		try {
			ArrayList<Employee> employees = repo.getEr().read();
			employees.add(emp);
			repo.getEr().write(employees);
			succesful = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return succesful;
	}

	public boolean deleteEmployee(Employee emp) {
		boolean succesful = false;
		ArrayList<Employee> employees;
		try {
			employees = repo.getEr().read();
			for (Employee e : employees) {
				if (emp.getId() == e.getId()) {
					e.setStatus(false);
					succesful = true;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return succesful;
	}

	public boolean addMember(Member memb) {
		boolean succesful = false;
		try {
			ArrayList<Member> members = repo.getMr().read();
			members.add(memb);
			repo.getMr().write(members);
			succesful = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return succesful;
	}

	public boolean deleteMember(Member memb) {
		boolean succesful = false;
		ArrayList<Member> members;
		try {
			members = repo.getMr().read();
			for (Member m : members) {
				if (memb.getId() == m.getId()) {
					m.setStatus(false);
					succesful = true;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return succesful;
	}

}
