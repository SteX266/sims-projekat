package services;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import classes.Author;
import classes.Book;
import classes.BookCopy;
import classes.Borrowing;
import classes.Employee;
import classes.Member;
import classes.Membership;
import classes.Publisher;
import classes.ReservationRequest;
import classes.Review;
import repository.Repositories;

public abstract class Service {
	
	Repositories repo;
	
	public Service() {
		 this.repo = new Repositories("resources/author.json","resources/book.json", "resources/bookcopy.json", "resources/borrowing.json", "resources/employee.json", "resources/member.json", "resources/membership.json", "resources/publisher.json", "resources/reservationrequest.json", "resources/review.json");
	}
	
	public ArrayList<Book> searchBooks(ArrayList<String> inputWords) throws JsonParseException, JsonMappingException, IOException{
		ArrayList<Book> filteredBooks = new ArrayList<Book>();
		ArrayList<Book> allBooks = (ArrayList<Book>) repo.getBr().read();
		
		for (Book book : allBooks) {
			Boolean isAdded = false;
			for (String word : inputWords) {
				word = word.toLowerCase();
				if (book.getTitle().toLowerCase().contains(word)) {
					filteredBooks.add(book);
					isAdded = true;
				}
				else if(book.getGenre().toString().toLowerCase().contains(word)) {
					filteredBooks.add(book);
					isAdded = true;
				}
				else {
					for (Author author : book.getAuthors()) {
						if(author.getName().toLowerCase().contains(word) || author.getSurname().toLowerCase().contains(word)) {
							filteredBooks.add(book);
							isAdded = true;
						}
					}
				}
				if (isAdded) {
					break;
				}
			}
		}
		return filteredBooks;
	}
	
	public ArrayList<Author> readAuthors(){
		try {
			return this.repo.getAr().read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public ArrayList<Book> readBooks(){
		try {
			return this.repo.getBr().read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public ArrayList<BookCopy> readBookCopies(){
		try {
			return this.repo.getBcr().read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public ArrayList<Borrowing> readBorrowings(){
		try {
			return this.repo.getBrwr().read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public ArrayList<Employee> readEmployees(){
		try {
			return this.repo.getEr().read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public ArrayList<Member> readMembers(){
		try {
			return this.repo.getMr().read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public ArrayList<Membership> readMemberships(){
		try {
			return this.repo.getMshr().read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public ArrayList<Publisher> readPublishers(){
		try {
			return this.repo.getPr().read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	public ArrayList<ReservationRequest> readReservationRequests(){
		try {
			return this.repo.getRrqr().read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	public ArrayList<Review> readReviews(){
		try {
			return this.repo.getRvr().read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public void writeAuthors(ArrayList<Author> authors) {
		this.repo.getAr().write(authors);
	}
	public void writeBooks(ArrayList<Book> books) {
		this.repo.getBr().write(books);
	}
	public void writeBookCopies(ArrayList<BookCopy> bookCopies) {
		this.repo.getBcr().write(bookCopies);
	}
	public void writeBorrowings(ArrayList<Borrowing> borrowings) {
		this.repo.getBrwr().write(borrowings);
	}
	public void writeEmployees(ArrayList<Employee> employees) {
		this.repo.getEr().write(employees);
	}
	public void writeMembers(ArrayList<Member> members) {
		this.repo.getMr().write(members);
	}
	public void writeMemberships(ArrayList<Membership>memberships) {
		this.repo.getMshr().write(memberships);
	}
	public void writePublishers(ArrayList<Publisher>publishers) {
		this.repo.getPr().write(publishers);
	}
	public void writeReservationRequests(ArrayList<ReservationRequest>reservationRequests) {
		this.repo.getRrqr().write(reservationRequests);
	}
	public void writeReviews(ArrayList<Review> reviews) {
		this.repo.getRvr().write(reviews);
	}
	
}
