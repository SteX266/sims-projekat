package services;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import classes.Employee;
import classes.Member;
import classes.MemberType;
import classes.Membership;
import classes.User;


public class StartService extends Service{

	public StartService() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public User login(String username, String password) {
		ArrayList<User> users = new ArrayList<User>();
		ArrayList<Member> members = new ArrayList<Member>();
		ArrayList<Employee> employees = new ArrayList<Employee>();
		User user = null;
		members = (ArrayList<Member>) readMembers();
		employees =(ArrayList<Employee>) readEmployees();
		users = mergeUsers(members, employees); 
		user = getUserByEmail(users, username);
		if (user == null) {
			return null;
		}
		else if (user.getPassword().equals(password)) {
			return user;
		}
		else {
			return null;
		}
		
	}
	private User getUserByEmail(ArrayList<User> users, String email) {
		for (User user : users) {
			if (user.getEmail().equals(email)) {
				return user;
			}
		}
		return null;
	}



	
	public ArrayList<User> mergeUsers(ArrayList<Member> members, ArrayList<Employee> employees){
		
		ArrayList<User> users = new ArrayList<User>();
		for (Employee employee : employees) {
			users.add(employee);
		}
		
		for (Member member : members) {
			users.add(member);
		}
		return users;
		
	}
	public boolean register(String name, String surname, String jmbg, LocalDate birthDate,String location,  String address, String email, String password, MemberType type) throws JsonParseException, JsonMappingException, IOException {
		
		ArrayList<Member> members = readMembers();
		Member lastMember = members.stream().collect(Collectors.maxBy(Comparator.comparingInt(Member::getId))).get();
		int id = lastMember.getId() + 1;
		LocalDate today = LocalDate.now();
		Membership membership = new Membership(today, today.plusMonths(1));
		Member m = new Member(id, name, surname, jmbg, birthDate, location, address, email, password, type, membership);
		members.add(m);
		ArrayList<Membership> memberships = readMemberships();
		memberships.add(membership);
		writeMemberships(memberships);
		writeMembers(members);
		return true;
		
		
		
	}
	

	
}
