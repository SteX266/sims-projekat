package services;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.JsonMappingException;

import classes.Author;
import classes.Book;
import classes.BookCopy;
import classes.BookStatus;
import classes.Borrowing;
import classes.Employee;
import classes.Member;
import classes.MemberType;
import classes.ReservationRequest;
import classes.Review;
import classes.User;
import repository.Repositories;

public class Functions {
	Repositories repo;
	
	
	public Functions(Repositories r) {

		this.repo = r;
		
	}
	
	public void extendReturnDate(Borrowing borrowing, LocalDate newDate) throws JsonParseException, JsonMappingException, IOException {
		ArrayList<Borrowing> borrowings = repo.getBrwr().read();
		
		for (Borrowing borrowing2 : borrowings) {
			if (borrowing.getId() == borrowing2.getId()){
				borrowing2.setReturnDate(newDate);
				break;
			}
		}
		repo.getBrwr().write(borrowings);
	}



	//Bibliotekar ucita id knjige - simuliranje
	public void returnBook(int bookCopyId, BookStatus status) throws JsonParseException, JsonMappingException, IOException {
		ArrayList<BookCopy> bookCopies = repo.getBcr().read();
		for (BookCopy bookCopy : bookCopies) {
			if (bookCopyId == bookCopy.getId()) {
				bookCopy.setStatus(status);	//Bibliotekar moze da oznaci da je knjiga slobodna/ostecena/izgubljena
				break;
			}
		}
		repo.getBcr().write(bookCopies);
	}
}
