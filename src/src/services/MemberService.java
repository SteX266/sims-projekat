package services;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import classes.Book;
import classes.BookCopy;
import classes.BookStatus;
import classes.Borrowing;
import classes.Member;
import classes.ReservationRequest;
import classes.Review;

public class MemberService extends UserService {

	public MemberService() {
		super();
	}

	public ArrayList<Borrowing> getBorrowingsByMember(Member member) {
		ArrayList<Borrowing> borrowings = member.getBorrowings();

		return borrowings;
	}

	public boolean reservation(int id, LocalDate date, Member member)
			throws JsonParseException, JsonMappingException, IOException {
		Book book = repo.getBr().findById(id);
		ArrayList<BookCopy> copies = getCopiesOfBook(book);
		for (BookCopy copy : copies) {
			if (copy.getStatus() == BookStatus.AVAILABLE) {
				ArrayList<ReservationRequest> requests = (ArrayList<ReservationRequest>) repo.getRrqr().read();
				ReservationRequest rq1 = requests.stream()
						.collect(Collectors.maxBy(Comparator.comparingInt(ReservationRequest::getId))).get();
				ReservationRequest rq = new ReservationRequest(rq1.getId() + 1, date, false, null, book, copy, member);
				requests.add(rq);
				repo.getRrqr().write(requests);
				ArrayList<Member> members = repo.getMr().read();
				for (Member member2 : members) {
					if (member.getId() == member2.getId()) {
						member2.addReservationRequest(rq);
						break;
					}
				}
				repo.getMr().write(members);
				return true;
			}
		}

		return false;
	}

	public ArrayList<BookCopy> getCopiesOfBook(Book book) throws JsonParseException, JsonMappingException, IOException {
		ArrayList<BookCopy> copies = new ArrayList<>();
		for (BookCopy cpy : repo.getBcr().read()) {
			if (cpy.getBook().equals(book)) {
				copies.add(cpy);
			}
		}
		return copies;
	}

	public void createReview(int score, Member member, Book book)
			throws JsonParseException, JsonMappingException, IOException {
		ArrayList<Review> reviews = repo.getRvr().read();
		Review lastReview = reviews.stream().collect(Collectors.maxBy(Comparator.comparingInt(Review::getId))).get();
		int id = lastReview.getId() + 1;

		Review r = new Review(id, score, member, book);
		reviews.add(r);
		repo.getRvr().write(reviews);

	}

	public void extendReturnDate(Borrowing borrowing, LocalDate newDate)
			throws JsonParseException, JsonMappingException, IOException {
		ArrayList<Borrowing> borrowings = repo.getBrwr().read();

		for (Borrowing borrowing2 : borrowings) {
			if (borrowing.getId() == borrowing2.getId()) {
				borrowing2.setReturnDate(newDate);
				break;
			}
		}
		repo.getBrwr().write(borrowings);
	}

}
