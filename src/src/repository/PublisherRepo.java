package repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import classes.Publisher;

public class PublisherRepo extends Repo {
	public PublisherRepo(String path) {
		super(path);
	}

	
	@SuppressWarnings("deprecation")
	public ArrayList<Publisher> read() throws JsonParseException, JsonMappingException, IOException {
		ArrayList<Publisher> lst = new ArrayList<Publisher>();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		mapper.registerModule(new JavaTimeModule());
		lst = mapper.readValue(new File(path), new TypeReference<ArrayList<Publisher>>(){});
		return lst;
	}
	public boolean write(List<Publisher> lst) {
		mapper.enable(SerializationFeature.INDENT_OUTPUT);

		try {
			mapper.writeValue(new File(path),lst);
		} catch (JsonGenerationException e) {
			return false;
		} catch (JsonMappingException e) {
			return false;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	public Publisher findById(int id) throws JsonParseException, JsonMappingException, IOException {
		List<Publisher> lst = read();
		for (Publisher p : lst) {
			if(p.getId() == id) {
				return p;
			}
		}
		return null;
	}
}
