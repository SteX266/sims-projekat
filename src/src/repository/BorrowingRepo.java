package repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import classes.Borrowing;
import classes.Member;
import jsonclasses.BorrowingJson;

public class BorrowingRepo extends Repo {
	
	private BookCopyRepo bcr;
	private MemberRepo mr;
	
	public BorrowingRepo(String path ,BookCopyRepo bcr) {
		super(path);
		this.bcr = bcr;
		}

	
	
	public MemberRepo getMr() {
		return mr;
	}



	public void setMr(MemberRepo mr) {
		this.mr = mr;
	}



	public ArrayList<Borrowing> jsonToClass(List<BorrowingJson> lst) throws JsonParseException, JsonMappingException, IOException{
		ArrayList<Borrowing> lst2 = new ArrayList<>();
		ArrayList<Member> members = mr.readMembersNoReservations();
		for (BorrowingJson cpy : lst) {
			for (Member member : members) {	
				if (member.getId() == cpy.getMemberId()) {
					lst2.add(new Borrowing(cpy.getId(), cpy.getBorrowingDate(),cpy.getReturnDate(), bcr.findById(cpy.getBookCopyId()),member));
					break;
				}
			}
		}
		return lst2;
		}
		
	public ArrayList<BorrowingJson> classToJson(List<Borrowing> lst) {
		ArrayList<BorrowingJson> lst2 = new ArrayList<BorrowingJson>();
			for (Borrowing cpy : lst) {
				lst2.add(new BorrowingJson(cpy.getId(), cpy.getBorrowingDate(),cpy.getReturnDate(), cpy.getBookCopy().getId(),cpy.getMember().getId()));
			}
			return lst2;
			}
		
		
	@SuppressWarnings("deprecation")
	public ArrayList<Borrowing> read() throws JsonParseException, JsonMappingException, IOException {
		ArrayList<BorrowingJson> lst = new ArrayList<>();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		mapper.registerModule(new JavaTimeModule());
		lst = mapper.readValue(new File(path), new TypeReference<ArrayList<BorrowingJson>>(){});
		return jsonToClass(lst);
	}
	
	public boolean write(List<Borrowing> lst) {
		mapper.enable(SerializationFeature.INDENT_OUTPUT);

		try {
			mapper.writeValue(new File(path),classToJson(lst));
		} catch (JsonGenerationException e) {
			return false;
		} catch (JsonMappingException e) {
			return false;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	public Borrowing findById(int id) throws JsonParseException, JsonMappingException, IOException {
		List<Borrowing> lst = read();
		for (Borrowing b : lst) {
			if(b.getId() == id) {
				return b;
			}
		}
		return null;
	}
}
