package repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import classes.Membership;

public class MembershipRepo extends Repo {
	public MembershipRepo(String path) {
		super(path);
	}

	
	@SuppressWarnings("deprecation")
	public ArrayList<Membership> read() throws JsonParseException, JsonMappingException, IOException {
		ArrayList<Membership> lst = new ArrayList<Membership>();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		mapper.registerModule(new JavaTimeModule());
		lst = mapper.readValue(new File(path), new TypeReference<ArrayList<Membership>>(){});
		return lst;
	}
	public boolean write(List<Membership> lst) {
		mapper.enable(SerializationFeature.INDENT_OUTPUT);

		try {
			mapper.writeValue(new File(path),lst);
		} catch (JsonGenerationException e) {
			return false;
		} catch (JsonMappingException e) {
			return false;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	public Membership findById(int id) throws JsonParseException, JsonMappingException, IOException {
		List<Membership> lst = read();
		for (Membership m : lst) {
			if(m.getId() == id) {
				return m;
			}
		}
		return null;
	}
}
