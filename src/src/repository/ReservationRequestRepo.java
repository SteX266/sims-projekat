package repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import classes.Borrowing;
import classes.Member;
import classes.ReservationRequest;
import jsonclasses.MemberJson;
import jsonclasses.ReservationRequestJson;

public class ReservationRequestRepo extends Repo {
	private BookRepo br;
	private BookCopyRepo bcr;
	private MemberRepo mr;
	public ReservationRequestRepo(String path, BookRepo br, BookCopyRepo bcr) {
		super(path);
		this.br = br;
		this.bcr = bcr;
		
	}
	
	public void setMr(MemberRepo mr) {
		this.mr = mr;
	}
	
	public ArrayList<ReservationRequest> jsonToClass(ArrayList<ReservationRequestJson> lst) throws JsonParseException, JsonMappingException, IOException{
		ArrayList<ReservationRequest> lst2 = new ArrayList<>();
		ArrayList<Member> members = mr.readMembersNoReservations();
	
		for (ReservationRequestJson cpy : lst) {
			for (Member member : members) {	
				if (member.getId() == cpy.getMemberId()) {
					lst2.add(new ReservationRequest(cpy.getId(), cpy.getRequestDate(),cpy.getIsApproved(),cpy.getApprovalDate(),br.findById(cpy.getBookId()),bcr.findById(cpy.getBookCopyId()),member));
					break;
				}
			}
		}
		return lst2;
		}
	
	
		public ArrayList<ReservationRequestJson> classToJson(List<ReservationRequest> lst) {
			ArrayList<ReservationRequestJson> lst2 = new ArrayList<>();
			for (ReservationRequest cpy : lst) {
				lst2.add(new ReservationRequestJson(cpy.getId(), cpy.getRequestDate(), cpy.getIsApproved(), cpy.getApprovalDate(), cpy.getBook().getId(), cpy.getBookCopy().getId(),cpy.getMember().getId()));
			}
			return lst2;
			}
		
		public ArrayList<Member> read(ArrayList<Member>  members){
			
			return members;
			
		}
		
	@SuppressWarnings("deprecation")
	public ArrayList<ReservationRequest> read() throws StreamReadException, DatabindException, IOException {
		ArrayList<ReservationRequestJson> lst = new ArrayList<>();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		mapper.registerModule(new JavaTimeModule());
		lst = mapper.readValue(new File(path), new TypeReference<ArrayList<ReservationRequestJson>>(){});
		return jsonToClass(lst);
	}
	public boolean write(List<ReservationRequest> lst) {
		mapper.enable(SerializationFeature.INDENT_OUTPUT);

		try {
			mapper.writeValue(new File(path),classToJson(lst));
		} catch (JsonGenerationException e) {
			return false;
		} catch (JsonMappingException e) {
			return false;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	public ReservationRequest findById(int id) throws StreamReadException, DatabindException, IOException {
		List<ReservationRequest> lst = read();
		for (ReservationRequest r : lst) {
			if(r.getId() == id) {
				return r;
			}
		}
		return null;
	}
}
