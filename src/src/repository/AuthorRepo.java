package repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;


import classes.Author;

public class AuthorRepo extends Repo {
	public AuthorRepo(String path) {
		super(path);
	}

	
	@SuppressWarnings("deprecation")
	public ArrayList<Author> read()  throws JsonParseException, JsonMappingException, IOException{
		ArrayList<Author> lst = new ArrayList<Author>();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		try {
			lst = mapper.readValue(new File(path), new TypeReference<ArrayList<Author>>(){});
		} catch (StreamReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lst;
	}
	public boolean write(ArrayList<Author> lst) {
		mapper.enable(SerializationFeature.INDENT_OUTPUT);

		try {
			mapper.writeValue(new File(path),lst);
		} catch (JsonGenerationException e) {
			return false;
		} catch (JsonMappingException e) {
			return false;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	public Author findById(int id) throws JsonParseException, JsonMappingException, IOException {
		ArrayList<Author> lst = read();
		for (Author author : lst) {
			if(author.getId() == id) {
				return author;
			}
		}
		return null;
	}
}