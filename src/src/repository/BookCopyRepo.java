package repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import classes.BookCopy;
import jsonclasses.BookCopyJson;

public class BookCopyRepo extends Repo {
	private BookRepo br;
	
	public BookCopyRepo(String path,BookRepo br) {
		super(path);
		this.br = br;
	}
	
	
	public ArrayList<BookCopy> jsonToClass(ArrayList<BookCopyJson> lst) throws JsonParseException, JsonMappingException, IOException{
	ArrayList<BookCopy> lst2 = new ArrayList<BookCopy>();
	for (BookCopyJson cpy : lst) {
		lst2.add(new BookCopy(cpy.getId(), cpy.getStatus(),br.findById(cpy.getBookId())));
	}
	return lst2;
	}
	
	public ArrayList<BookCopyJson> classToJson(ArrayList<BookCopy> lst) {
		ArrayList<BookCopyJson> lst2 = new ArrayList<BookCopyJson>();
		for (BookCopy cpy : lst) {
			lst2.add(new BookCopyJson(cpy.getId(), cpy.getStatus(),cpy.getBook().getId()));
		}
		return lst2;
		}
	
	@SuppressWarnings("deprecation")
	public ArrayList<BookCopy> read() throws JsonParseException, JsonMappingException, IOException  {
		ArrayList<BookCopyJson> lst = new ArrayList<BookCopyJson>();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		mapper.registerModule(new JavaTimeModule());
		try {
			lst = mapper.readValue(new File(path), new TypeReference<ArrayList<BookCopyJson>>(){});
		} catch (StreamReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatabindException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonToClass(lst);
	}
	
	public boolean write(ArrayList<BookCopy> lst2) {
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		ArrayList<BookCopyJson> lst = classToJson(lst2);
		try {
			mapper.writeValue(new File(path),lst);
		} catch (JsonGenerationException e) {
			return false;
		} catch (JsonMappingException e) {
			return false;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	public BookCopy findById(int id) throws JsonParseException, JsonMappingException, IOException {
		ArrayList<BookCopy> lst = read();
		for (BookCopy bc : lst) {
			if(bc.getId() == id) {
				return bc;
			}
		}
		return null;
	}
}
