package repository;

public class Repositories {
	private AuthorRepo ar;
	private BookRepo br;
	private BookCopyRepo bcr;
	private BorrowingRepo brwr;
	private EmployeeRepo er;
	private MemberRepo mr;
	private MembershipRepo mshr;
	private PublisherRepo pr;
	private ReservationRequestRepo rrqr;
	private ReviewRepo rvr;
	
	public Repositories() {
		super();
	}

	public Repositories(AuthorRepo ar, BookRepo br, BookCopyRepo bcr, BorrowingRepo brwr, EmployeeRepo er,
			MemberRepo mr, MembershipRepo mshr, PublisherRepo pr, ReservationRequestRepo rrqr, ReviewRepo rvr) {
		super();
		this.ar = ar;
		this.br = br;
		this.bcr = bcr;
		this.brwr = brwr;
		this.er = er;
		this.mr = mr;
		this.mshr = mshr;
		this.pr = pr;
		this.rrqr = rrqr;
		this.rvr = rvr;
	}
	public Repositories(String ars, String brs, String bcrs, String brwrs, String ers,
			String mrs, String mshrs, String prs, String rrqrs, String rvrs) {
		super();
		this. er = new EmployeeRepo(ers);
		this.ar = new AuthorRepo(ars);
		this.pr = new PublisherRepo(prs);
		this.br = new BookRepo(brs,ar,pr);
		this.bcr = new BookCopyRepo(bcrs,br);
		this.brwr = new BorrowingRepo(brwrs,bcr);
		this.mshr = new MembershipRepo(mshrs);
		this.rrqr = new ReservationRequestRepo(rrqrs,br,bcr);
		this.mr = new MemberRepo(mrs,mshr,brwr,rrqr);
		this.rvr = new ReviewRepo(rvrs,mr,br);
		this.rrqr.setMr(mr);
		this.brwr.setMr(mr);
	}

	public AuthorRepo getAr() {
		return ar;
	}

	public BookRepo getBr() {
		return br;
	}

	public BookCopyRepo getBcr() {
		return bcr;
	}

	public BorrowingRepo getBrwr() {
		return brwr;
	}

	public EmployeeRepo getEr() {
		return er;
	}

	public MemberRepo getMr() {
		return mr;
	}

	public MembershipRepo getMshr() {
		return mshr;
	}

	public PublisherRepo getPr() {
		return pr;
	}

	public ReservationRequestRepo getRrqr() {
		return rrqr;
	}

	public ReviewRepo getRvr() {
		return rvr;
	}
	
	
}
