package repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import classes.Author;
import classes.Book;
import jsonclasses.BookJson;

public class BookRepo extends Repo {
	private AuthorRepo ar;
	private PublisherRepo pr;
	
	public BookRepo(String path,AuthorRepo ar,PublisherRepo pr) {
		super(path);
		this.ar =ar;
		this.pr = pr;
	}

	
	private ArrayList<Book> jsonToClass(List<BookJson> lst) throws JsonParseException, JsonMappingException, IOException{
		ArrayList<Book> lst2 = new ArrayList<Book>();
		for (BookJson cpy : lst) {
			ArrayList<Author> authors = new ArrayList<Author>();
			for (int a : cpy.getAuthorsIds()) {
				authors.add(ar.findById(a));
			}
			lst2.add(new Book(cpy.getId(), cpy.getISBN(), cpy.getTitle(), cpy.getPublishingDate(), cpy.getPrintDate(), cpy.getGenre(), pr.findById(cpy.getPublisherId()), authors));
		}
		return lst2;
		}
	
	private ArrayList<BookJson> classToJson(List<Book> lst){
		ArrayList<BookJson> lst2 = new ArrayList<>();
		for (Book cpy : lst) {
			ArrayList<Integer> authors = new ArrayList<>();
			for (Author a : cpy.getAuthors()) {
				authors.add(a.getId());
			}
			lst2.add(new BookJson(cpy.getId(),cpy.getISBN(), cpy.getTitle(), cpy.getPublishingDate(), cpy.getPrintDate(), cpy.getPublisher().getId(), cpy.getGenre(), authors));
		}
		return lst2;
		}

	
	@SuppressWarnings("deprecation")
	public ArrayList<Book> read() throws JsonParseException, JsonMappingException, IOException {
		ArrayList<BookJson> lst = new ArrayList<>();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		mapper.registerModule(new JavaTimeModule());
		lst = mapper.readValue(new File(path), new TypeReference<ArrayList<BookJson>>(){});
		return jsonToClass(lst);
	}
	public boolean write(List<Book> lst) {
		mapper.enable(SerializationFeature.INDENT_OUTPUT);

		try {
			mapper.writeValue(new File(path),classToJson(lst));
		} catch (JsonGenerationException e) {
			return false;
		} catch (JsonMappingException e) {
			return false;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	public Book findById(int id) throws JsonParseException, JsonMappingException, IOException {
		List<Book> lst = read();
		for (Book b : lst) {
			if(b.getId() == id) {
				return b;
			}
		}
		return null;
	}
}
