package repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import classes.Review;
import jsonclasses.ReviewJson;

public class ReviewRepo extends Repo {
	private MemberRepo mr;
	private BookRepo br;
	
	public ReviewRepo(String path, MemberRepo mr, BookRepo br) {
		super(path);
		this.mr = mr;
		this.br = br;
	}

	

	public ArrayList<Review> jsonToClass(List<ReviewJson> lst) throws JsonParseException, JsonMappingException, IOException{
		ArrayList<Review> lst2 = new ArrayList<>();
		for (ReviewJson cpy : lst) {
			lst2.add(new Review(cpy.getId(), cpy.getScore(), mr.findById(cpy.getMemberId()), br.findById(cpy.getBookId())));
		}
		return lst2;
		}
		
		public List<ReviewJson> classToJson(List<Review> lst) {
			List<ReviewJson> lst2 = new ArrayList<>();
			for (Review cpy : lst) {
				lst2.add(new ReviewJson(cpy.getId(), cpy.getScore(), cpy.getMember().getId(), cpy.getBook().getId()));
			}
			return lst2;
			}
		
	@SuppressWarnings("deprecation")
	public ArrayList<Review> read() throws JsonParseException, JsonMappingException, IOException {
		List<ReviewJson> lst = new ArrayList<>();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		mapper.registerModule(new JavaTimeModule());
		lst = mapper.readValue(new File(path), new TypeReference<List<ReviewJson>>(){});
		return jsonToClass(lst);
	}
	public boolean write(List<Review> lst) {
		mapper.enable(SerializationFeature.INDENT_OUTPUT);

		try {
			mapper.writeValue(new File(path),classToJson(lst));
		} catch (JsonGenerationException e) {
			return false;
		} catch (JsonMappingException e) {
			return false;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	public Review findById(int id) throws JsonParseException, JsonMappingException, IOException {
		List<Review> lst = read();
		for (Review r : lst) {
			if(r.getId() == id) {
				return r;
			}
		}
		return null;
	}
}
