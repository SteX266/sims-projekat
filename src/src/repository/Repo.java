package repository;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;



public abstract class Repo{
	protected String path;
	protected ObjectMapper mapper;
	public Repo(String path) {
		super();
		this.path = path;
		this.mapper = new ObjectMapper();
	}

	abstract public ArrayList<?> read()  throws JsonParseException, JsonMappingException, IOException;
}