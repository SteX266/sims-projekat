package repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import classes.Borrowing;
import classes.Member;
import classes.ReservationRequest;
import jsonclasses.MemberJson;

public class MemberRepo extends Repo {
	MembershipRepo msr;
	BorrowingRepo brwr;
	ReservationRequestRepo rrqr;
	public MemberRepo(String path, MembershipRepo msr, BorrowingRepo brwr,ReservationRequestRepo rrqr) {
		super(path);
		this.msr = msr;
		this.brwr = brwr;
		this.rrqr = rrqr;
	}

	
	public  ArrayList<Member> readMembersNoReservations() throws StreamReadException, DatabindException, IOException{
		ArrayList<MemberJson> lst = new ArrayList<>();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		mapper.registerModule(new JavaTimeModule());
		lst = mapper.readValue(new File(path), new TypeReference<ArrayList<MemberJson>>(){});
		ArrayList<Member> lst2 = new ArrayList<>();
		for (MemberJson cpy : lst) {
			ArrayList<Borrowing> borrowings = new  ArrayList<>();
			ArrayList<ReservationRequest> requests = new  ArrayList<>();
		lst2.add(new Member(cpy.getId(), cpy.getName(), cpy.getSurname(), cpy.getJmbg(),cpy.getBirthDate(),cpy.getLocation(),cpy.getAddress(),cpy.getEmail(),cpy.getPassword(), cpy.getStatus(),cpy.getType(), msr.findById(cpy.getMembershipId()), borrowings, requests));
	}
		return lst2;

	}
	
	
	public ArrayList<Member> jsonToClass(List<MemberJson> lst) throws JsonParseException, JsonMappingException, IOException{
        ArrayList<Member> lst2 = new ArrayList<>();
        ArrayList<ReservationRequest> requests = rrqr.read();
        ArrayList<Borrowing> borrowings = brwr.read();
        for (MemberJson cpy : lst) {
            ArrayList<Borrowing> borrowingsMember = new ArrayList<>(); ;
            for (Borrowing borrowing : borrowings ) {
        	   if (borrowing.getMember().getId() == cpy.getId()) {
        		   borrowingsMember.add(borrowing);
    	}}
            ArrayList<ReservationRequest> requestsMember = new ArrayList<>(); ;
            for (ReservationRequest request : requests ) {
        	   if (request.getMember().getId()== cpy.getId()) {
        		   requestsMember.add(request);
		}}
            lst2.add(new Member(cpy.getId(), cpy.getName(), cpy.getSurname(), cpy.getJmbg(),cpy.getBirthDate(),cpy.getLocation(),cpy.getAddress(),cpy.getEmail(),cpy.getPassword(), cpy.getStatus(),cpy.getType(), msr.findById(cpy.getMembershipId()), borrowings, requests));
        }
        return lst2;
        }
		
	public ArrayList<MemberJson> classToJson(List<Member> lst) {
			ArrayList<MemberJson> lst2 = new ArrayList<>();
			for (Member cpy : lst) {
				ArrayList<Integer> borrowings = new  ArrayList<>();
				for (Borrowing x : cpy.getBorrowings()) {
					borrowings.add(x.getId());
				}
				ArrayList<Integer> requests = new  ArrayList<>();
				for (ReservationRequest x : cpy.getReservationRequests()) {
					requests.add(x.getId());
				}
				lst2.add(new MemberJson(cpy.getId(), cpy.getName(), cpy.getSurname(), cpy.getJmbg(),cpy.getBirthDate(),cpy.getLocation(),cpy.getAddress(),cpy.getEmail(),cpy.getPassword(), cpy.getStatus(),cpy.getType(), cpy.getMembership().getId()));
			}
			
			return lst2;
			}
	

	@SuppressWarnings("deprecation")
	public ArrayList<Member> read() throws JsonParseException, JsonMappingException, IOException {
		ArrayList<MemberJson> lst = new ArrayList<>();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		mapper.registerModule(new JavaTimeModule());
		lst = mapper.readValue(new File(path), new TypeReference<ArrayList<MemberJson>>(){});
		return jsonToClass(lst);
	}
	
	public boolean write(List<Member> lst) {
		mapper.enable(SerializationFeature.INDENT_OUTPUT);

		try {
			mapper.writeValue(new File(path),classToJson(lst));
		} catch (JsonGenerationException e) {
			return false;
		} catch (JsonMappingException e) {
			return false;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	public Member findById(int id) throws JsonParseException, JsonMappingException, IOException {
		List<Member> lst = read();
		for (Member m : lst) {
			if(m.getId() == id) {
				return m;
			}
		}
		return null;
	}
	
}

