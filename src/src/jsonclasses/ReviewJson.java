package jsonclasses;

import classes.Review;

public class ReviewJson extends Review{
	private int memberId;
	private int bookId;
	
	public ReviewJson() {
		super();
		
	}
	public ReviewJson(int id, int score, int member, int book) {
		super();
		this.id = id;
		this.score = score;
		this.memberId = member;
		this.bookId = book;
	}
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	
}
