package jsonclasses;

import java.time.LocalDate;


import classes.Member;
import classes.MemberType;

public class MemberJson extends Member {
	private int membershipId;
	
	
	public MemberJson() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MemberJson(int id, String name, String surname, String jmbg, LocalDate birthDate, String location,
			String address, String email, String password, Boolean status, MemberType type, int membership) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.jmbg = jmbg;
		this.birthDate = birthDate;
		this.location = location;
		this.address = address;
		this.email = email;
		this.password = password;
		this.status = status;
		this.type = type;
		this.membershipId = membership;
	}
	public int getMembershipId() {
		return membershipId;
	}
	public void setMembershipId(int membershipId) {
		this.membershipId = membershipId;
	}

	
}
