package jsonclasses;


import java.time.LocalDate;

import classes.ReservationRequest;

public class ReservationRequestJson extends ReservationRequest {
	private int bookId;
	private int bookCopyId;
	private int memberId;
	
	public ReservationRequestJson() {
		super();
	}
	public ReservationRequestJson(int id, LocalDate requestDate, Boolean isApproved, LocalDate approvalDate,
			int book, int bookCopy, int memberId) {
		super();
		this.id = id;
		this.requestDate = requestDate;
		this.isApproved = isApproved;
		this.approvalDate = approvalDate;
		this.bookId = book;
		this.bookCopyId = bookCopy;
		this.memberId = memberId;
	}

	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public int getBookCopyId() {
		return bookCopyId;
	}
	public void setBookCopyId(int bookCopyId) {
		this.bookCopyId = bookCopyId;
	}
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	@Override
	public String toString() {
		return "ReservationRequestJson [bookId=" + bookId + ", bookCopyId=" + bookCopyId + ", memberId=" + memberId
				+ ", id=" + id + ", requestDate=" + requestDate + ", isApproved=" + isApproved + ", approvalDate="
				+ approvalDate + "]";
	}

	
	
}
