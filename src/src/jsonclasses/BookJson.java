package jsonclasses;

import java.time.LocalDate;
import java.util.ArrayList;

import classes.Book;
import classes.BookGenre;

public class BookJson extends Book{
	private int  publisherId;
	private ArrayList<Integer> authorsIds;
	
	public BookJson() {
		super();
	}

	public BookJson( int id,String iSBN, String title, LocalDate publishingDate, LocalDate printDate, int publisherId,
	BookGenre genre, ArrayList<Integer> authorsIds) {
		super();
		this.id = id;
		this.ISBN = iSBN;
		this.title = title;
		this.publishingDate = publishingDate;
		this.printDate = printDate;
		this.publisherId = publisherId;
		this.genre = genre;
		this.authorsIds = authorsIds;
	}

	public int getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(int publisherId) {
		this.publisherId = publisherId;
	}

	public ArrayList<Integer> getAuthorsIds() {
		return authorsIds;
	}

	public void setAuthorsIds(ArrayList<Integer> authorsIds) {
		this.authorsIds = authorsIds;
	}

	
}