package jsonclasses;

import classes.BookCopy;
import classes.BookStatus;

public class BookCopyJson extends BookCopy{
	private int bookId;
	
	
	public BookCopyJson() {
		super();
	}

	public BookCopyJson(int id, BookStatus status,int bookId) {
		super();
		this.bookId = bookId;
		this.id = id;
		this.status = status;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}	
	

}