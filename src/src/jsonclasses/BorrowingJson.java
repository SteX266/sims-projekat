package jsonclasses;

import java.time.LocalDate;

import classes.Borrowing;

public class BorrowingJson extends Borrowing {
	private int bookCopyId;
	private int memberId;
	
	public BorrowingJson() {
		super();
	}
	
	public BorrowingJson(int id, LocalDate borrowingDate, LocalDate returnDate, int bookCopy, int memberId) {
		super();
		this.id = id;
		this.borrowingDate = borrowingDate;
		this.returnDate = returnDate;
		this.bookCopyId = bookCopy;
		this.memberId = memberId;

	}

	public int getBookCopyId() {
		return bookCopyId;
	}

	public void setBookCopyId(int bookCopyId) {
		this.bookCopyId = bookCopyId;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	
}
