package gui.dialogs;

import javax.swing.JDialog;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;

public class BookDialog extends JDialog {

	public BookDialog() {
		super();
		setAlwaysOnTop(true);
		setLocation(500, 300);
		setSize(425,300);
		getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 16));
		getContentPane().setLayout(null);

		confirmButton = new JButton("Confirm");
		confirmButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		confirmButton.setBounds(300, 210, 100, 30);
		getContentPane().add(confirmButton);

		titleField = new JTextField();
		titleField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		titleField.setBounds(10, 50, 100, 30);
		getContentPane().add(titleField);
		titleField.setColumns(10);

		JLabel titleLabel = new JLabel("Title");
		titleLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		titleLabel.setBounds(10, 10, 100, 30);
		getContentPane().add(titleLabel);

		JLabel publisherLabel = new JLabel("Publisher");
		publisherLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		publisherLabel.setBounds(150, 10, 100, 30);
		getContentPane().add(publisherLabel);

		JLabel genreLabel = new JLabel("Genre");
		genreLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		genreLabel.setBounds(300, 10, 100, 30);
		getContentPane().add(genreLabel);

		JComboBox month = new JComboBox();
		month.setFont(new Font("Tahoma", Font.PLAIN, 16));
		month.setBounds(290, 130, 40, 30);
		getContentPane().add(month);

		JComboBox day = new JComboBox();
		day.setFont(new Font("Tahoma", Font.PLAIN, 16));
		day.setBounds(240, 130, 40, 30);
		getContentPane().add(day);

		JComboBox year = new JComboBox();
		year.setFont(new Font("Tahoma", Font.PLAIN, 16));
		year.setBounds(340, 130, 60, 30);
		getContentPane().add(year);

		JComboBox monthp = new JComboBox();
		monthp.setFont(new Font("Tahoma", Font.PLAIN, 16));
		monthp.setBounds(60, 130, 40, 30);
		getContentPane().add(monthp);

		JComboBox dayP = new JComboBox();
		dayP.setFont(new Font("Tahoma", Font.PLAIN, 16));
		dayP.setBounds(10, 130, 40, 30);
		getContentPane().add(dayP);

		JComboBox yearP = new JComboBox();
		yearP.setFont(new Font("Tahoma", Font.PLAIN, 16));
		yearP.setBounds(110, 130, 60, 30);
		getContentPane().add(yearP);

		JLabel lblPublishingDate = new JLabel("Publishing Date");
		lblPublishingDate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPublishingDate.setBounds(10, 90, 120, 30);
		getContentPane().add(lblPublishingDate);

		JLabel lblPrintDate = new JLabel("Print Date");
		lblPrintDate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPrintDate.setBounds(240, 90, 120, 30);
		getContentPane().add(lblPrintDate);

		JComboBox publisherBox = new JComboBox();
		publisherBox.setBounds(150, 50, 100, 30);
		getContentPane().add(publisherBox);

		JComboBox genreBox = new JComboBox();
		genreBox.setBounds(300, 50, 100, 30);
		getContentPane().add(genreBox);

		JLabel lblAuthor = new JLabel("Author");
		lblAuthor.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAuthor.setBounds(10, 170, 100, 30);
		getContentPane().add(lblAuthor);

		addButton = new JButton("Add");
		addButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		addButton.setBounds(120, 210, 60, 30);
		getContentPane().add(addButton);

		authorField = new JTextField();
		authorField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		authorField.setColumns(10);
		authorField.setBounds(10, 210, 100, 30);
		getContentPane().add(authorField);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7633569154557181582L;
	protected JTextField titleField;
	protected JTextField authorField;
	protected JButton confirmButton;
	protected JButton addButton;
}
