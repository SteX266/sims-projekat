package gui.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class BookAddDialog extends BookDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2611031327979544537L;

	public BookAddDialog() {
		super();
		confirmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		ArrayList<String> authors = new ArrayList<String>();
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				authors.add(authorField.getText());
				authorField.setText("");
			}
		});

	}
}
