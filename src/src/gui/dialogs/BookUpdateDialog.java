package gui.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class BookUpdateDialog extends BookDialog {
	public BookUpdateDialog() {
		super();
		authors = new ArrayList<String>();
		confirmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				authors.add(authorField.getText());
				authorField.setText("");
			}

		});
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1337593967526414816L;

	private ArrayList<String> authors;

}
