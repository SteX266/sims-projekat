package gui.dialogs;

import javax.swing.JDialog;
import javax.swing.JTextField;

import gui.screens.Screen;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BookFindDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2376246026433528571L;
	private JTextField textField;
	private Screen screen;
	public BookFindDialog(Screen s) {
		super();
		screen = s;
		getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField.setBounds(10, 10, 400, 30);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Find");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(420, 10, 100, 30);
		getContentPane().add(btnNewButton);

	}

}
