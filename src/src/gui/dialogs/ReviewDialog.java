package gui.dialogs;

import javax.swing.JDialog;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;

import classes.Book;
import classes.Member;
import gui.screens.MemberScreen;

public class ReviewDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2229727085303542790L;
	private MemberScreen screen;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBox;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ReviewDialog(MemberScreen s, Book book) {
		super();
		screen = s;
		getContentPane().setLayout(null);
		setSize(180, 160);
		setLocation(600, 400);

		JButton btnNewButton = new JButton("Make Review");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					screen.getService().createReview(Integer.valueOf(comboBox.getSelectedItem().toString()),
							(Member) screen.getGuiManager().getUser(), book);
				} catch (NumberFormatException | IOException e1) {
					e1.printStackTrace();
				}
				// ovde treba da se stavi kod za review
				dispose();
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(10, 83, 150, 30);
		getContentPane().add(btnNewButton);

		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5" }));
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 16));
		comboBox.setBounds(120, 10, 40, 30);
		getContentPane().add(comboBox);

		JLabel lblNewLabel = new JLabel("Rating");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(10, 10, 100, 30);
		getContentPane().add(lblNewLabel);
	}
}
