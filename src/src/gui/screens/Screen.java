package gui.screens;

import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import classes.User;
import gui.GuiManager;
import gui.panels.AdsPanel;
import gui.panels.NewsPanel;
import services.Service;

public class Screen extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected GuiManager guiManager = null;
	private AdsPanel leftPanel = null;
	private AdsPanel rightPanel = null;
	protected JPanel mainPanel = null;
	public JFrame frame = null;
	protected JMenuBar menuBar = null;
	protected JMenu menu = null;
	protected Service service;

	public Screen(GuiManager gm) {
		super();
		guiManager = gm;
		frame = gm.getFrame();
		setSize(Toolkit.getDefaultToolkit().getScreenSize());
		createMenuBar();
		setLayout(null);
		addAdSpace();
		changeMainPanel(new NewsPanel(this));
	}
	public Service getService() {
		return service;
	}
	protected void createMenuBar() {
		menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		menu = new JMenu("Actions");
		menu.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		menuBar.add(menu);
	}

	protected void addAdSpace() {
		leftPanel = new AdsPanel();
		rightPanel = new AdsPanel();
		rightPanel.setLocation(1270, 10);
		leftPanel.setLocation(10, 10);
		add(leftPanel);
		add(rightPanel);
	}

	public void changeMainPanel(JPanel p) {
		if (mainPanel != null) {
			remove(mainPanel);
			mainPanel = p;
			mainPanel.setLocation(280, 10);
			add(mainPanel);
		} else {
			mainPanel = p;
			mainPanel.setLocation(280, 10);
			add(mainPanel);
		}
		resetScreen();
	}

	public void setGuiManager(GuiManager gm) {
		guiManager = gm;
	}

	public GuiManager getGuiManager() {
		return guiManager;
	}

	public void changeUser(User u) {
		guiManager.setUser(u);
	}

	public void resetScreen() {
		setVisible(false);
		setVisible(true);
	}

}
