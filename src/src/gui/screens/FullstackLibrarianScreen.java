package gui.screens;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import gui.GuiManager;
import gui.panels.BookCrudPanel;
import gui.panels.ChangePasswordPanel;
import gui.panels.IssueBookPanel;
import gui.panels.MembershipPanel;
import gui.panels.NewsPanel;
import gui.panels.RegisterPanel;
import gui.panels.LibrarianReservationsPanel;
import services.LibrarianService;

public class FullstackLibrarianScreen extends Screen {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8245706808058517404L;
	private LibrarianService service;
	public FullstackLibrarianScreen(GuiManager gm) {
		super(gm);
		setService(new LibrarianService());
		addMenuItems();
	}

	private void addMenuItems() {

		JMenuItem startPage = new JMenuItem("Start page");
		startPage.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		startPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new NewsPanel(guiManager.getScreen()));
			}
		});
		menu.add(startPage);

		JMenuItem changePassword = new JMenuItem("Change password");
		changePassword.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		changePassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new ChangePasswordPanel(guiManager.getScreen()));
			}
		});
		menu.add(changePassword);

		JMenuItem register = new JMenuItem("Register user");
		register.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		register.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new RegisterPanel(guiManager.getScreen()));
			}
		});
		menu.add(register);

		JMenuItem reservations = new JMenuItem("Reservations");
		reservations.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		reservations.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new LibrarianReservationsPanel(guiManager.getScreen()));
			}
		});
		menu.add(reservations);
		
		JMenuItem issueBook = new JMenuItem("Issue book");
		issueBook.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		issueBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new IssueBookPanel(guiManager.getScreen()));
			}
		});
		menu.add(issueBook);
		

		JMenuItem extendMembership = new JMenuItem("Extend membership");
		extendMembership.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		extendMembership.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new MembershipPanel(guiManager.getScreen()));
			}
		});
		menu.add(extendMembership);

		JMenuItem bookCrud = new JMenuItem("Book CRUD");
		bookCrud.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		bookCrud.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new BookCrudPanel(guiManager.getScreen()));
			}
		});
		menu.add(bookCrud);

		JMenuItem mntmNewMenuItem = new JMenuItem("Logout");
		mntmNewMenuItem.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menu.add(mntmNewMenuItem);
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guiManager.changeScreen(new StartScreen(guiManager));
			}
		});
	}

	public LibrarianService getService() {
		return service;
	}

	public void setService(LibrarianService service) {
		this.service = service;
	}
}
