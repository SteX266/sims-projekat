package gui.screens;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import gui.GuiManager;
import gui.panels.ChangePasswordPanel;
import gui.panels.NewsPanel;

public class InterpreterScreen extends Screen {

	/**
	 * 
	 */
	private static final long serialVersionUID = -561227524967494497L;

	public InterpreterScreen(GuiManager gm) {
		super(gm);
		// TODO Auto-generated constructor stub
	}
	private void addMenuItems() {

		JMenuItem startPage = new JMenuItem("Start page");
		startPage.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		startPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new NewsPanel(guiManager.getScreen()));
			}
		});
		menu.add(startPage);

		JMenuItem changePassword = new JMenuItem("Change password");
		changePassword.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		changePassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new ChangePasswordPanel(guiManager.getScreen()));
			}
		});
		menu.add(changePassword);
		
		JMenuItem bookCrud = new JMenuItem("Book CRUD");
		bookCrud.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		bookCrud.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new ChangePasswordPanel(guiManager.getScreen()));
			}
		});
		menu.add(bookCrud);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Logout");
		mntmNewMenuItem.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menu.add(mntmNewMenuItem);
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guiManager.changeScreen(new StartScreen(guiManager));
			}
		});
	}
}
