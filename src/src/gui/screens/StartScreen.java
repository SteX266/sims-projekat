package gui.screens;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import gui.GuiManager;
import gui.panels.LoginPanel;
import gui.panels.NewsPanel;
import gui.panels.SearchPanel;
import services.StartService;

public class StartScreen extends Screen {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2191383677154073954L;
	private StartService service;

	public StartScreen(GuiManager gm) {
		super(gm);
		service = new StartService();
		changeMainPanel(new NewsPanel(this));
		addMenuItems();
	}

	public StartService getService() {
		return this.service;
	}

	private void addMenuItems() {

		JMenuItem startPage = new JMenuItem("Start page");
		startPage.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		startPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new NewsPanel(guiManager.getScreen()));
			}
		});
		menu.add(startPage);

		JMenuItem login = new JMenuItem("Login");
		login.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new LoginPanel(guiManager.getScreen()));
			}
		});
		menu.add(login);

		JMenuItem search = new JMenuItem("Search");
		search.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menu.add(search);
		search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new SearchPanel(guiManager.getScreen()));
			}
		});
	}

}
