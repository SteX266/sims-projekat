package gui.screens;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import gui.GuiManager;
import gui.panels.BookCrudPanel;
import gui.panels.ChangePasswordPanel;
import gui.panels.EmployeeCrudPanel;
import gui.panels.MemberCrudPanel;
import gui.panels.NewsPanel;
import services.AdministratorService;

public class AdminScreen extends Screen {

	/**
	 * 
	 */
	private static final long serialVersionUID = 499585901286193313L;
	private AdministratorService service;

	public AdminScreen(GuiManager gm) {
		super(gm);
		service = new AdministratorService();
		addMenuItems();
	}

	public AdministratorService getService() {
		return service;
	}

	private void addMenuItems() {

		JMenuItem startPage = new JMenuItem("Start page");
		startPage.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		startPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new NewsPanel(guiManager.getScreen()));
			}
		});
		menu.add(startPage);

		JMenuItem changePassword = new JMenuItem("Change password");
		changePassword.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		changePassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new ChangePasswordPanel(guiManager.getScreen()));
			}
		});
		menu.add(changePassword);
		
		JMenuItem bookCrud = new JMenuItem("Book CRUD");
		bookCrud.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		bookCrud.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new BookCrudPanel(guiManager.getScreen()));
			}
		});
		menu.add(bookCrud);

		JMenuItem employeeCrud = new JMenuItem("Employee CRUD");
		employeeCrud.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		employeeCrud.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new EmployeeCrudPanel(guiManager.getScreen()));
			}
		});
		menu.add(employeeCrud);

		JMenuItem memberCrud = new JMenuItem("Member CRUD");
		memberCrud.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		memberCrud.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new MemberCrudPanel(guiManager.getScreen()));
			}
		});
		menu.add(memberCrud);

		JMenuItem logout = new JMenuItem("Logout");
		logout.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menu.add(logout);
		logout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guiManager.changeScreen(new StartScreen(guiManager));
			}
		});
	}

}
