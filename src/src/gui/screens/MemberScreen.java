package gui.screens;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import gui.GuiManager;
import gui.panels.MakeReservationPanel;
import gui.panels.BorrowingsPanel;
import gui.panels.ChangePasswordPanel;
import gui.panels.NewsPanel;
import gui.panels.UserReservationsPanel;
import services.MemberService;

public class MemberScreen extends Screen {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6693081081115460412L;
	private MemberService service;

	public MemberScreen(GuiManager gm) {
		super(gm);
		service = new MemberService();
		addMenuItems();
	}

	public MemberService getService() {
		return this.service;
	}

	private void addMenuItems() {

		JMenuItem startPage = new JMenuItem("Start page");
		startPage.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		startPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new NewsPanel(guiManager.getScreen()));
			}
		});
		menu.add(startPage);

		JMenuItem changePassword = new JMenuItem("Change password");
		changePassword.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		changePassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new ChangePasswordPanel(guiManager.getScreen()));
			}
		});
		menu.add(changePassword);

		JMenuItem reservation = new JMenuItem("Search");
		reservation.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		reservation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new MakeReservationPanel(guiManager.getScreen()));
			}
		});
		menu.add(reservation);

		JMenuItem reservations = new JMenuItem("Reservations");
		reservations.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		reservations.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new UserReservationsPanel(guiManager.getScreen())); // treba dodati reservation panel
			}
		});
		menu.add(reservations);

		JMenuItem borrowings = new JMenuItem("Borrowings");
		borrowings.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menu.add(borrowings);
		borrowings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeMainPanel(new BorrowingsPanel(guiManager.getScreen()));
			}
		});
		JMenuItem logout = new JMenuItem("Logout");
		logout.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menu.add(logout);
		logout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guiManager.changeScreen(new StartScreen(guiManager));
			}
		});
	}
}
