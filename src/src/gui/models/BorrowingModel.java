package gui.models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;


import classes.Borrowing;

public class BorrowingModel  extends AbstractTableModel{
	private static final long serialVersionUID = 1L;
	private String[] columnNames = {"Borrowing Date","ReturnDate"," Book","Copy", "Member"};
	public List<Borrowing>  borrowings = new ArrayList<>();

	public BorrowingModel(List<Borrowing> borrowings) {
		super();
		this.borrowings = borrowings;
	}
	public String[] getColumnNames() {
		return columnNames;
	}
	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}
	public List<Borrowing> getBorrowings() {
		return borrowings;
	}
	public void setBorrowings(List<Borrowing> borrowings) {
		this.borrowings = borrowings;
	}
	
	@Override
	public int getRowCount() {
		return borrowings.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return this.columnNames[col];
	}



	@Override
	public Object getValueAt(int row, int column) {
		Borrowing b = (Borrowing) borrowings.get(row);
		return b.toCell(column);

	}
}

