package gui.models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import classes.Author;

public class AuthorModel extends AbstractTableModel{
	private static final long serialVersionUID = 1L;
	private String[] columnNames = {"Name","Surname"};
	public List<Author>  authors = new ArrayList<>(); 

	public AuthorModel(List<Author> authors) {
		super();
		this.authors = authors;
	}
	public String[] getColumnNames() {
		return columnNames;
	}
	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}
	public List<Author> getAuthors() {
		return authors;
	}
	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	@Override
	public int getRowCount() {
		return authors.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return this.columnNames[col];
	}



	@Override
	public Object getValueAt(int row, int column) {
		Author a = (Author) authors.get(row);
		return a.toCell(column);

	}
}