package gui.models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import classes.ReservationRequest;

public class RequestModel  extends AbstractTableModel{

	private static final long serialVersionUID = 1L;
	private String[] columnNames = {"Book","Copy","Member","Request Date","ApprovalDate","Is Approved"};
	public List<ReservationRequest>  requests = new ArrayList<>();
	
	
	public RequestModel(List<ReservationRequest> requests) {
		super();
		
		this.requests = requests;
	}
	public String[] getColumnNames() {
		return columnNames;
	}
	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}
	public List<ReservationRequest> getRequests() {
		return requests;
	}
	public void setRequests(List<ReservationRequest> requests) {
		this.requests = requests;
	}
	@Override
	public int getRowCount() {
		return requests.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return this.columnNames[col];
	}
	
	@Override
	public Object getValueAt(int row, int column) {
		ReservationRequest rr = (ReservationRequest) requests.get(row);
		return rr.toCell(column);

	}
	

}
