package gui.models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import classes.Book;


public class BookModel extends AbstractTableModel{

	private static final long serialVersionUID = 1L;
	private String[] columnNames = {"Title","Publishing Date","Print Date","Genre","Publisher","Authors","ISBN",};
	public List<Book> books  = new ArrayList<>();


	public BookModel(List<Book> books) {
		super();
		this.books = books;
	}

	public String[] getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int getRowCount() {
		return books.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int row, int column) {
		Book b = (Book) books.get(row);
		return b.toCell(column);
	}

	@Override
	public String getColumnName(int col) {
		return this.columnNames[col];
	}


}  

