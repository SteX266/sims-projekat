package gui.models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import classes.Employee;

public class EmployeeModel extends AbstractTableModel{

	private static final long serialVersionUID = 1L;
	private String[] columnNames = {"Name","Surname"," Email","jmbg","Birth Date","Type" ,"salary"};
	public List<Employee>  employees = new ArrayList<>(); 
		


	public EmployeeModel(List<Employee> employees) {
		super();
		this.employees = employees;
	}



	public String[] getColumnNames() {
		return columnNames;
	}



	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}



	public List<Employee> getEmployees() {
		return employees;
	}



	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	@Override
	public int getRowCount() {
		return employees.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return this.columnNames[col];
	}



	@Override
	public Object getValueAt(int row, int column) {
		Employee e = (Employee) employees.get(row);
		return e.toCell(column);

	}
}