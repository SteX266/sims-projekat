package gui.models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import classes.Member;

public class MemberModel  extends AbstractTableModel{

	private static final long serialVersionUID = 1L;
	private String[] columnNames = {"Name","Surname"," Email","jmbg","Birth Date","Type"};
	public List<Member>  members = new ArrayList<>();
	
	
	public MemberModel(List<Member> members) {
		super();
		this.members = members;
	}
	
	

	public String[] getColumnNames() {
		return columnNames;
	}



	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}



	public List<Member> getMembers() {
		return members;
	}



	public void setMembers(List<Member> members) {
		this.members = members;
	}



	@Override
	public int getRowCount() {
		return members.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return this.columnNames[col];
	}



	@Override
	public Object getValueAt(int row, int column) {
		Member m = (Member) members.get(row);
		return m.toCell(column);

	}
}
