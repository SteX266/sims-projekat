package gui;

import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;

import classes.Employee;
import classes.User;
import gui.screens.FullstackLibrarianScreen;
import gui.screens.InterpreterScreen;
import gui.screens.LibrarianScreen;
import gui.screens.MemberScreen;
import gui.screens.Screen;
import gui.screens.*;

public class GuiManager {
	private JFrame frame = null;
	private Screen currentScreen = null;
	private User currentUser = null;

	public GuiManager() {
		createFrame();
		changeScreen(new StartScreen(this));
	}

	public JFrame getFrame() {
		return frame;
	}

	public Screen getScreen() {
		return currentScreen;

	}

	public void showGui() {
		frame.setVisible(true);
	}

	public void changeScreen(Screen p) {
		currentScreen = p;
		connectFrameAndScreen();
		currentScreen.resetScreen();
	}

	public Screen getScreenByUser(User u) {
		if ("classes.Member".equals(u.getClass().getName())) {
			return new MemberScreen(this);
		} else {
			Employee e = (Employee) u;
			switch (e.getType()) {
			case LIBRARIAN:
				return new LibrarianScreen(this);
			case ADMINISTRATOR:
				return new AdminScreen(this);
			case INTERPRETER:
				return new InterpreterScreen(this);
			case FULL_STACK:
				return new FullstackLibrarianScreen(this);
			default:
				throw new IllegalArgumentException("Unexpected value: " + e.getType());
			}
		}

	}

	private void createFrame() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		Image icon = Toolkit.getDefaultToolkit().getImage("resources/icon-book.png");    
		frame.setIconImage(icon);  
	}

	private void connectFrameAndScreen() {
		frame.setContentPane(currentScreen);
	}
	
	public void setUser(User u) {
		currentUser = u;
	}
	
	public User getUser() {
		return currentUser;
	}

}
