package gui.panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDate;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;

import classes.Member;
import classes.User;
import gui.screens.Screen;
import services.StartService;
import classes.MemberType;

public class RegisterPanel extends MainPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3755797676960079571L;
	private JTextField nameTxtField;
	private JTextField surnameTxtField;
	private JPasswordField passwordTxtField;
	private JTextField emailTxtField;
	private JTextField jmbgTxtField;
	private JTextField addressTxtField;
	public JButton registerButton;
	public JComboBox memberTypeCB = new JComboBox();
	public JComboBox cityCB = new JComboBox();
	public JComboBox datCB = new JComboBox();
	public JComboBox monthCB = new JComboBox();
	public JComboBox yearCB = new JComboBox();


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public RegisterPanel(Screen s) {
		super(s);

		JPanel panel = new JPanel();
		panel.setBounds(320, 100, 340, 500);
		add(panel);
		panel.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		panel.setLayout(null);

		JLabel nameLabel = new JLabel("Name");
		nameLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		nameLabel.setBounds(10, 10, 150, 30);
		panel.add(nameLabel);

		nameTxtField = new JTextField();
		nameTxtField.setColumns(10);
		nameTxtField.setBounds(10, 40, 155, 30);
		panel.add(nameTxtField);

		JLabel surnameLabel = new JLabel("Surname");
		surnameLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		surnameLabel.setBounds(175, 10, 155, 30);
		panel.add(surnameLabel);

		surnameTxtField = new JTextField();
		surnameTxtField.setColumns(10);
		surnameTxtField.setBounds(175, 40, 155, 30);
		panel.add(surnameTxtField);

		JLabel passwordLabel = new JLabel("Password");
		passwordLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		passwordLabel.setBounds(10, 79, 155, 30);
		panel.add(passwordLabel);

		passwordTxtField = new JPasswordField();
		passwordTxtField.setBounds(10, 109, 155, 30);
		panel.add(passwordTxtField);

		emailTxtField = new JTextField();
		emailTxtField.setColumns(10);
		emailTxtField.setBounds(10, 180, 320, 30);
		panel.add(emailTxtField);

		JLabel emailLabel = new JLabel("Email Address");
		emailLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		emailLabel.setBounds(10, 150, 150, 30);
		panel.add(emailLabel);

		jmbgTxtField = new JTextField();
		jmbgTxtField.setColumns(10);
		jmbgTxtField.setBounds(10, 250, 155, 30);
		panel.add(jmbgTxtField);

		JLabel jmbgLabel = new JLabel("JMBG");
		jmbgLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		jmbgLabel.setBounds(10, 220, 150, 30);
		panel.add(jmbgLabel);

		JLabel cityLabel = new JLabel("City");
		cityLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cityLabel.setBounds(170, 220, 155, 30);
		panel.add(cityLabel);

		cityCB.setMaximumRowCount(5);
		cityCB.setModel(
				new DefaultComboBoxModel(new String[] {"Novi Sad", "Beograd", "Zrenjanin", "Nis", "Jagodina"}));
		cityCB.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cityCB.setBounds(175, 250, 155, 30);
		panel.add(cityCB);

		JLabel addressLabel = new JLabel("Address");
		addressLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		addressLabel.setBounds(10, 289, 150, 30);
		panel.add(addressLabel);

		addressTxtField = new JTextField();
		addressTxtField.setColumns(10);
		addressTxtField.setBounds(10, 320, 260, 30);
		panel.add(addressTxtField);

		datCB.setModel(new DefaultComboBoxModel(
				new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
						"17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
		datCB.setMaximumRowCount(5);
		datCB.setFont(new Font("Tahoma", Font.PLAIN, 16));
		datCB.setBounds(10, 390, 50, 30);
		panel.add(datCB);

		monthCB.setModel(new DefaultComboBoxModel(new String[] { "January", "Febuary", "Mar", "Apr", "May", "June",
				"July", "Avgust", "September", "October", "November", "December" }));
		monthCB.setMaximumRowCount(5);
		monthCB.setFont(new Font("Tahoma", Font.PLAIN, 16));
		monthCB.setBounds(70, 390, 170, 30);
		panel.add(monthCB);

		yearCB.setModel(new DefaultComboBoxModel(new String[] { "1990", "1991", "1992", "1993", "1994", "1995",
				"1996", "1997", "1998", "1999", "2000" }));
		yearCB.setMaximumRowCount(5);
		yearCB.setFont(new Font("Tahoma", Font.PLAIN, 16));
		yearCB.setBounds(250, 390, 80, 30);
		panel.add(yearCB);

		JLabel birthdayLabel = new JLabel("Birthday");
		birthdayLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		birthdayLabel.setBounds(10, 360, 150, 30);
		panel.add(birthdayLabel);

		registerButton = new JButton("Register");
		registerButton.setBounds(90, 460, 150, 30);
		panel.add(registerButton);
		registerButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JLabel lblMembertype = new JLabel("Member Type");
		lblMembertype.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMembertype.setBounds(175, 81, 155, 30);
		panel.add(lblMembertype);
		
		
		memberTypeCB.setModel(new DefaultComboBoxModel(MemberType.values()));
		memberTypeCB.setMaximumRowCount(5);
		memberTypeCB.setFont(new Font("Tahoma", Font.PLAIN, 16));
		memberTypeCB.setBounds(175, 109, 155, 30);
		panel.add(memberTypeCB);
		setRegisterOperation();
	}

	private void setRegisterOperation() {
		registerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = nameTxtField.getText();
				String surname = surnameTxtField.getText();
				String jmbg = jmbgTxtField.getText();
				String email = emailTxtField.getText();
				String password = getPassword();
				String address = addressTxtField.getText();
				String location = cityCB.getSelectedItem().toString();
				MemberType memberType = (MemberType) memberTypeCB.getSelectedItem();
				int month = monthCB.getSelectedIndex() + 1;
				int day = Integer.valueOf(datCB.getSelectedItem().toString());
				int year = Integer.valueOf(yearCB.getSelectedItem().toString());
				LocalDate date = LocalDate.of(year, month, day);
				
				boolean successful = false;
				try {
					successful = ((StartService) screen.getService()).register(name, surname, jmbg, date, location, address, email, password, memberType);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}// pokreni iz servisa registraciju
				if (!successful) {
					JOptionPane.showMessageDialog(null, "Couldn't register!", "REGISTRATION ERROR!", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "Registration successful", "SUCCESS", JOptionPane.INFORMATION_MESSAGE);
					changeToStart();
				}
				
			}
		});
	}

	private void changeToStart() {
		screen.changeMainPanel(new NewsPanel(screen));
	}

	public User getUserDto() {
		return new Member();
	}
	
	private String getPassword() {
		StringBuilder sb = new StringBuilder();
		sb.append(passwordTxtField.getPassword());
		return sb.toString();
	}
}
