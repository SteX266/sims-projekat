package gui.panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;

public class AdsPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AdsPanel() {
		super();
		setLayout(null);
		setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		setSize(260, 750);
		BufferedImage maxbet = null;
		try {
			maxbet = ImageIO.read(new File("resources/reklama.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JLabel picLabel = new JLabel(new ImageIcon(maxbet));
		picLabel.setLocation(10, 40);
		picLabel.setSize(240, 493);
		add(picLabel);
		
		BufferedImage africkaSljiva = null;
		try {
			africkaSljiva = ImageIO.read(new File("resources/reklama1.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		picLabel.setLocation(10, 40);
		picLabel.setSize(240, 493);

		JPanel panel_1_1_1 = new JPanel();
		panel_1_1_1.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		panel_1_1_1.setBounds(10, 40, 240, 700);
		add(panel_1_1_1);
		panel_1_1_1.setLayout(null);
		JLabel picLabel2 = new JLabel(new ImageIcon(africkaSljiva));
		picLabel2.setBounds(0, 493, 240, 207);
		panel_1_1_1.add(picLabel2);

		JLabel lblAds_1 = new JLabel("Ads");
		lblAds_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblAds_1.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblAds_1.setBounds(10, 10, 200, 30);
		add(lblAds_1);
	}

}
