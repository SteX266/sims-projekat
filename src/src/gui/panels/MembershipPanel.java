package gui.panels;

import gui.screens.Screen;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.MatteBorder;
import java.awt.Color;

public class MembershipPanel extends MainPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6756701488987305614L;
	private JTextField textField;
	private JTextField textField_1;

	public MembershipPanel(Screen s) {
		super(s);
		
		JPanel panel = new JPanel();
		panel.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		panel.setBounds(295, 130, 390, 130);
		add(panel);
		panel.setLayout(null);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField_1.setColumns(10);
		textField_1.setBounds(230, 10, 150, 30);
		panel.add(textField_1);
		
		textField = new JTextField();
		textField.setBounds(230, 50, 150, 30);
		panel.add(textField);
		textField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Membership star date");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(10, 10, 210, 30);
		panel.add(lblNewLabel);
		
		JLabel lblMembershipEndDate = new JLabel("Membership expiration date");
		lblMembershipEndDate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMembershipEndDate.setBounds(10, 50, 210, 30);
		panel.add(lblMembershipEndDate);
		
		JButton btnNewButton = new JButton("Extend");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(230, 90, 150, 30);
		panel.add(btnNewButton);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Monthly", "Yearly"}));
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 16));
		comboBox.setBounds(130, 90, 90, 30);
		panel.add(comboBox);
		
		JLabel lblExtentionType = new JLabel("Extention type");
		lblExtentionType.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblExtentionType.setBounds(10, 90, 110, 30);
		panel.add(lblExtentionType);
		
	}
}
