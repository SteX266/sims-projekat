package gui.panels;

import gui.models.MemberModel;
import gui.screens.Screen;
import services.AdministratorService;

import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import classes.Member;

import java.awt.event.ActionEvent;

public class MemberCrudPanel extends CrudPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7527809611986784796L;
	private MemberModel model;
	
	public MemberCrudPanel(Screen s) {
		super(s);
		ArrayList<Member> members = screen.getService().readMembers();
		model = new MemberModel(screen.getService().readMembers());
		table = new JTable(model);
		model.fireTableDataChanged();
		scrollPane.getViewport().add(table);
		findButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				Member m = members.get(row);
				((AdministratorService) screen.getService()).deleteMember(m);
			}
		});
		updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		model = new MemberModel(screen.getService().readMembers());
		table.setModel(model);
		model.fireTableDataChanged();
	}

}
