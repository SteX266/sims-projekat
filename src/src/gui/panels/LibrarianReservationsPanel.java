package gui.panels;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import classes.BookCopy;
import classes.ReservationRequest;
import gui.screens.Screen;
import services.LibrarianService;

public class LibrarianReservationsPanel extends ReservationsPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1569463005305541350L;

	public LibrarianReservationsPanel(Screen s) {
		super(s);

		JButton btnGrantReservation = new JButton("Grant");
		btnGrantReservation.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnGrantReservation.setBounds(719, 627, 150, 30);
		add(btnGrantReservation);

		JButton btnHandOut = new JButton("Hand out");

		btnHandOut.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnHandOut.setBounds(559, 627, 150, 30);
		add(btnHandOut);

		model.setRequests(screen.getService().readReservationRequests());
		model.fireTableDataChanged();
		btnHandOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				if (row == -1) {
					JOptionPane.showMessageDialog(null, "You must select row", "Warning", JOptionPane.WARNING_MESSAGE);
				} else {
					ReservationRequest rr = model.getRequests().get(row);
					if (!rr.getIsApproved()) {
						JOptionPane.showMessageDialog(null, "Book wasn't approved!", "WARDING",
								JOptionPane.WARNING_MESSAGE);
					} else {
						BookCopy bookCopy = ((LibrarianService) screen.getService())
								.findAvailableBookCopy(rr.getBook());
						((LibrarianService) screen.getService()).deleteReservationRequest(rr);
						((LibrarianService) screen.getService()).createBorrowing(rr.getMember(), bookCopy);
						model.setRequests(((LibrarianService) screen.getService()).readReservationRequests());
						model.fireTableDataChanged();
						JOptionPane.showMessageDialog(null, "Book is handed out successfully!", "BORROWING STARTED",
								JOptionPane.INFORMATION_MESSAGE);
					}
				}

			}
		});
		btnGrantReservation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				if (row == -1) {
					JOptionPane.showMessageDialog(null, "You must select row", "Warning", JOptionPane.WARNING_MESSAGE);
				} else {
					ReservationRequest rr = model.getRequests().get(row);
					if (rr.getIsApproved() == true) {
						JOptionPane.showMessageDialog(null, "This reservation is already approved!", "ALREADY APPROVED",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						((LibrarianService) screen.getService()).approveReservationRequest(rr);
						model.setRequests(((LibrarianService) screen.getService()).readReservationRequests());
						model.fireTableDataChanged();
						JOptionPane.showMessageDialog(null, "Reservation approved successfully", "RESERVATION APPROVED",
								JOptionPane.INFORMATION_MESSAGE);
					}

				}
			}
		});
	}
}
