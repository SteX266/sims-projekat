package gui.panels;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;

import classes.ReservationRequest;
import gui.models.RequestModel;
import gui.screens.Screen;

public class ReservationsPanel extends MainPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 347477495847949666L;
	protected JTable table;
	protected RequestModel model = new RequestModel(new ArrayList<ReservationRequest>());
	public ReservationsPanel(Screen s) {
		super(s);
		table = new JTable(model);
		JScrollPane scrollPane = new JScrollPane(table);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(69, 67, 800, 550);
		add(scrollPane);

		JLabel lblNewLabel = new JLabel("Requests");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(70, 27, 200, 30);
		add(lblNewLabel);
	}

}
