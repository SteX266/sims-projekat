package gui.panels;

import gui.panels.MainPanel;

import gui.screens.Screen;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.ScrollPaneConstants;

public class CrudPanel extends MainPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3454257491017656460L;
	protected JTable table;
	protected JButton addButton;
	protected JButton findButton;
	protected JButton deleteButton;
	protected JButton updateButton;
	protected JScrollPane scrollPane;
	public CrudPanel(Screen s) {
		super(s);
		table = new JTable();

		scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(120, 50, 700, 500);
		add(scrollPane);

		findButton = new JButton("Find");
		findButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		findButton.setBounds(700, 560, 85, 30);
		add(findButton);

		deleteButton = new JButton("Delete");
		deleteButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		deleteButton.setBounds(590, 560, 85, 30);
		add(deleteButton);

		updateButton = new JButton("Update");
		updateButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		updateButton.setBounds(260, 560, 85, 30);
		add(updateButton);

		addButton = new JButton("Add");
		addButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		addButton.setBounds(150, 560, 85, 30);
		add(addButton);
	}
}
