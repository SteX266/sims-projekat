package gui.panels;

import gui.models.EmployeeModel;
import gui.screens.Screen;
import services.AdministratorService;

import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JTable;

import classes.Employee;

import java.awt.event.ActionEvent;

public class EmployeeCrudPanel extends CrudPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4388776921561654511L;
	private EmployeeModel model;
	public EmployeeCrudPanel(Screen s) {
		super(s);
		ArrayList<Employee> employees = screen.getService().readEmployees();
		model = new EmployeeModel(employees);
		table = new JTable(model);
		model.fireTableDataChanged();
		scrollPane.getViewport().add(table);
		findButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				Employee e1 = employees.get(row);
				((AdministratorService) screen.getService()).deleteEmployee(e1);
			}
		});
		updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		model = new EmployeeModel(screen.getService().readEmployees());
		table.setModel(model);
		model.fireTableDataChanged();
	}

}
