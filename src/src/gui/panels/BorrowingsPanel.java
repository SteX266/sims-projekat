package gui.panels;

import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import classes.Borrowing;
import classes.Member;
import gui.dialogs.ReviewDialog;
import gui.models.BorrowingModel;
import gui.screens.MemberScreen;
import gui.screens.Screen;
import services.MemberService;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDate;
import java.awt.event.ActionEvent;

public class BorrowingsPanel extends MainPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4409710601577994988L;
	private JTable table;

	public BorrowingsPanel(Screen s) {
		super(s);
		ArrayList<Borrowing> borrowings = ((MemberService) screen.getService())
				.getBorrowingsByMember((Member) screen.getGuiManager().getUser());
		BorrowingModel borrowingModel = new BorrowingModel(borrowings);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(123, 100, 700, 540);
		add(scrollPane);
		table = new JTable(borrowingModel);
		scrollPane.setViewportView(table);
		table.setFillsViewportHeight(true);

		JButton btnNewButton = new JButton("Create Review");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				Borrowing b = borrowings.get(row);
				ReviewDialog rd = new ReviewDialog((MemberScreen) screen, b.getBookCopy().getBook());
				rd.setVisible(true);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(513, 662, 150, 30);
		add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Extend");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				try {
					((MemberService) screen.getService()).extendReturnDate(borrowings.get(row), LocalDate.now().plusMonths(1));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton_1.setBounds(673, 662, 150, 30);
		add(btnNewButton_1);

		JLabel lblNewLabel = new JLabel("Borrowings");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(123, 60, 150, 30);
		add(lblNewLabel);
	}
}
