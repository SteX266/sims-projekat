package gui.panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.MatteBorder;

import classes.User;
import gui.screens.Screen;
import services.MemberService;

public class ChangePasswordPanel extends MainPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4874894279552929604L;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JPasswordField passwordField_2;

	public ChangePasswordPanel(Screen s) {
		super(s);
		setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		panel.setBounds(330, 150, 320, 170);
		add(panel);
		panel.setLayout(null);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(165, 10, 150, 30);
		panel.add(passwordField);
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JLabel lblNewLabel = new JLabel("Current Password");
		lblNewLabel.setBounds(10, 10, 150, 30);
		panel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(165, 50, 150, 30);
		panel.add(passwordField_1);
		passwordField_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JLabel lblNewPassword = new JLabel("New Password");
		lblNewPassword.setBounds(10, 50, 150, 30);
		panel.add(lblNewPassword);
		lblNewPassword.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		passwordField_2 = new JPasswordField();
		passwordField_2.setBounds(165, 90, 150, 30);
		panel.add(passwordField_2);
		passwordField_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JLabel lblOldPassword = new JLabel("Confirm Password");
		lblOldPassword.setBounds(10, 90, 150, 30);
		panel.add(lblOldPassword);
		lblOldPassword.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JButton btnNewButton = new JButton("Confirm");
		btnNewButton.setBounds(90, 130, 150, 30);
		panel.add(btnNewButton);
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String oldPassword = getPassword(passwordField);
				String newPassword1 = getPassword(passwordField_1);
				String newPassword2 = getPassword(passwordField_2);
				if (!newPassword1.equals(newPassword2)) {
					JOptionPane.showMessageDialog(null, "Password does not match!", "Password ERROR!", JOptionPane.ERROR_MESSAGE);
				}
				User u = screen.getGuiManager().getUser();
				
				
				try {
					boolean isSuccess = ((MemberService)screen.getService()).changePassword(u, newPassword1, oldPassword);
					if (isSuccess) {
						JOptionPane.showMessageDialog(null, "Change of password was successful!", "SUCCESS", JOptionPane.INFORMATION_MESSAGE);
					}
					else {
						JOptionPane.showMessageDialog(null, "Current password is not correct!", "Password ERROR!", JOptionPane.ERROR_MESSAGE);
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
	private String getPassword(JPasswordField field) {
		StringBuilder sb = new StringBuilder();
		sb.append(field.getPassword());
		return sb.toString();
	}
}
