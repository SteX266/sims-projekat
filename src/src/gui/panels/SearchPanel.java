package gui.panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.MatteBorder;

import classes.Book;
import gui.models.BookModel;
import gui.screens.Screen;

public class SearchPanel extends MainPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -652993433765501490L;
	protected JTextField txtSearch;
	protected JTable table;
	protected BookModel bookModel;
	
	public SearchPanel(Screen s) {
		super(s);

		JPanel panel = new JPanel();
		panel.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		panel.setBounds(10, 10, 960, 690);
		add(panel);
		panel.setLayout(null);

		txtSearch = new JTextField();
		txtSearch.setBounds(110, 10, 500, 30);
		panel.add(txtSearch);
		txtSearch.setToolTipText("");
		txtSearch.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtSearch.setColumns(10);

		JButton btnSearch = new JButton("Search");
		btnSearch.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnSearch.setBounds(620, 10, 100, 30);
		panel.add(btnSearch);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		panel_1.setBounds(110, 50, 840, 630);
		panel.add(panel_1);
		panel_1.setLayout(null);

		bookModel = new BookModel(screen.getService().readBooks());
		
		table = new JTable(bookModel);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		table.setFillsViewportHeight(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setBounds(10, 11, 820, 614);
		panel_1.add(scrollPane);

		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String txt = txtSearch.getText();
				ArrayList<String> text = new ArrayList<String>();
				text.add(txt);
				ArrayList<Book> books = null;
				try {
					books = screen.getService().searchBooks(text);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				bookModel.setBooks(books);
				bookModel.fireTableDataChanged();
			}
		});

	}

}
