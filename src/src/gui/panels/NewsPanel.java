package gui.panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;

import gui.screens.Screen;

public class NewsPanel extends MainPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NewsPanel(Screen s) {
		super(s);
		JLabel lblNewLabel = new JLabel("News");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(380, 10, 200, 30);
		add(lblNewLabel);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		panel_1.setBounds(10, 40, 960, 700);
		add(panel_1);
		panel_1.setLayout(null);
		BufferedImage icon = null;
		try {
			icon = ImageIO.read(new File("resources/biblioteka.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		JLabel slika = new JLabel(new ImageIcon(icon));
		slika.setBounds(0, 0, 960, 700);
		panel_1.add(slika);
	}
}
