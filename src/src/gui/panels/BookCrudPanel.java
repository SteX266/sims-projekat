package gui.panels;

import gui.dialogs.BookAddDialog;
import gui.dialogs.BookFindDialog;
import gui.dialogs.BookUpdateDialog;
import gui.models.BookModel;
import gui.screens.Screen;
import services.AdministratorService;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import classes.Book;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class BookCrudPanel extends CrudPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6362575601114353800L;
	private BookModel model;

	public BookCrudPanel(Screen s) {
		super(s);
		ArrayList<Book> books = screen.getService().readBooks();
		model = new BookModel(books);
		table = new JTable(model);
		model.fireTableDataChanged();
		scrollPane.getViewport().add(table);
		findButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BookFindDialog bfd = new BookFindDialog(screen);
				bfd.setVisible(true);
			}
		});
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int choice = JOptionPane.showConfirmDialog(null, "Confirm delete.", "Delete?",
						JOptionPane.YES_NO_OPTION);
				if (choice == JOptionPane.YES_OPTION) {
					int row = table.getSelectedRow();
					Book b = books.get(row);
					((AdministratorService) screen.getService()).deleteBook(b);
				}
			}
		});
		updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BookUpdateDialog bud = new BookUpdateDialog();
				bud.setVisible(true);
			}
		});
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BookAddDialog bad = new BookAddDialog();
				bad.setVisible(true);
			}
		});

		model = new BookModel(screen.getService().readBooks());
		table.setModel(model);

		JButton addCopyButton = new JButton("Add Copy");
		addCopyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		addCopyButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		addCopyButton.setBounds(410, 560, 100, 30);
		add(addCopyButton);
		model.fireTableDataChanged();

	}

}
