package gui.panels;

import java.awt.Font;

import javax.swing.JButton;

import classes.Member;
import classes.ReservationRequest;
import gui.screens.Screen;
import services.MemberService;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UserReservationsPanel extends ReservationsPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7436296374684444164L;

	public UserReservationsPanel(Screen s) {
		super(s);
		JButton btnNewButton = new JButton("Cancel");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				if (row == -1) {
				} else {
					MemberService ms = (MemberService)screen.getService();
					int id  = (int) table.getModel().getValueAt(row, -1);
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(719, 627, 150, 30);
		add(btnNewButton);
		
		model.setRequests(((Member) screen.getGuiManager().getUser()).getReservationRequests());
        model.fireTableDataChanged();
        
	}

}
