package gui.panels;

import gui.screens.Screen;
import services.LibrarianService;

import javax.swing.JTextField;

import classes.BookCopy;
import classes.Member;

import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.border.MatteBorder;
import java.awt.Color;
public class IssueBookPanel extends MainPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -266222576285559743L;
	private JTextField emailField;
	private JTextField idField;

	public IssueBookPanel(Screen s) {
		super(s);
		
		JPanel panel = new JPanel();
		panel.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		panel.setBounds(350, 230, 280, 150);
		add(panel);
		panel.setLayout(null);
		
		JButton btnNewButton = new JButton("Issue");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int id = Integer.parseInt(idField.getText());
					String email = emailField.getText();
					ArrayList<Member> members = screen.getService().readMembers();
					Member user = null;
					for (int i = 0; i < members.size(); i++) {
						if (email.equals(members.get(i).getEmail())) {
							user = members.get(i);
						}
					}
					
					ArrayList<BookCopy> books = screen.getService().readBookCopies();
					BookCopy book = null;
					for (int i = 0; i < books.size(); i++) {
						if (id == books.get(i).getId()) {
							book = books.get(i);
						}
					}
					LibrarianService serv = (LibrarianService) screen.getService();
					serv.createBorrowing(user,book);
					screen.changeMainPanel(new NewsPanel(screen));
					
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, "ERROR");
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(90, 110, 100, 30);
		panel.add(btnNewButton);
		
		idField = new JTextField();
		idField.setBounds(167, 50, 100, 30);
		panel.add(idField);
		idField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		idField.setColumns(10);
		
		JLabel lblCopyId = new JLabel("Book ID");
		lblCopyId.setBounds(57, 50, 100, 30);
		panel.add(lblCopyId);
		lblCopyId.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		emailField = new JTextField();
		emailField.setBounds(117, 10, 150, 30);
		panel.add(emailField);
		emailField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		emailField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Email");
		lblNewLabel.setBounds(10, 10, 100, 30);
		panel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
	}
}
