package gui.panels;

import gui.screens.Screen;
import services.MemberService;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import classes.Member;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDate;

public class MakeReservationPanel extends SearchPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7615655041791938417L;

	public MakeReservationPanel(Screen s) {
		super(s);

		JButton btnReservation = new JButton("Make Reservation");
		btnReservation.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnReservation.setBounds(790, 710, 180, 30);
		add(btnReservation);
		
		btnReservation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				if(row == -1) {
					JOptionPane.showMessageDialog(null, "You must select row", "Warning", JOptionPane.WARNING_MESSAGE);
				}else {
					int id  = (int) table.getModel().getValueAt(row, -1);
					MemberService ms = (MemberService)screen.getService();
					boolean x = false;
					try {
						x = ms.reservation(id, LocalDate.now(), (Member)screen.getGuiManager().getUser());
					} catch (IOException e1) {
						e1.printStackTrace();
					}	
					if(x) {
						String msg = "Request for book :" + table.getModel().getValueAt(row, 0).toString() + "\n" + "You will get aproval message very soon";
						JOptionPane.showMessageDialog(null,msg, "Successfull request ", JOptionPane.INFORMATION_MESSAGE);
					}
					else {
						String msg = "Request for book :" + table.getModel().getValueAt(row, 0).toString() + "\n" + "There is some problems with your request";
						JOptionPane.showMessageDialog(null,msg, "Unsuccessfull request ", JOptionPane.WARNING_MESSAGE);
					}
				}}});
			}
}
