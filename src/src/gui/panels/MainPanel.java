package gui.panels;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

import gui.screens.Screen;

public class MainPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -230855228696117941L;
	protected Screen screen;
	public MainPanel(Screen s) {
		super();
		this.screen = s;
		setSize(980, 750);
		setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		setLayout(null);
	}

}
