package gui.panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;

import classes.User;
import gui.screens.Screen;
import services.StartService;

public class LoginPanel extends MainPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8886335892729462534L;
	private JTextField textField;
	private JPasswordField passwordField;
	private JButton loginButton;

	public LoginPanel(Screen s) {
		super(s);
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		panel_1.setBounds(10, 40, 960, 700);
		panel_1.setLayout(null);
		add(panel_1);

		createButtons(panel_1);

		JPanel panel = new JPanel();
		panel.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		panel.setBounds(361, 145, 220, 250);
		panel_1.add(panel);
		panel.setLayout(null);

		textField = new JTextField();
		textField.setBounds(10, 50, 200, 30);
		panel.add(textField);
		textField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField.setColumns(10);

		JLabel lblNewLabel = new JLabel("Email");
		lblNewLabel.setBounds(10, 10, 150, 30);
		panel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 90, 150, 30);
		panel.add(lblPassword);
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 16));

		passwordField = new JPasswordField();
		passwordField.setBounds(10, 130, 200, 30);
		panel.add(passwordField);
		loginButton = new JButton("Login");
		loginButton.setBounds(60, 170, 150, 30);
		panel.add(loginButton);
		loginButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		loginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				User newUser = login(getUsername(), getPassword()); // poziv logina od servisa
				if (newUser == null) {
					JOptionPane.showMessageDialog(null, "Couldn't log in!", "LOGIN ERROR!", JOptionPane.ERROR_MESSAGE);
				} else {
					screen.changeUser(newUser);
					screen.getGuiManager().changeScreen(screen.getGuiManager().getScreenByUser(newUser));
				}
			}
		});

		screen.frame.getRootPane().setDefaultButton(loginButton);
	}

	private void createButtons(JPanel p) {
	}

	private String getUsername() {
		return textField.getText();
	}

	private String getPassword() {
		StringBuilder sb = new StringBuilder();
		sb.append(passwordField.getPassword());
		return sb.toString();
	}

	private User login(String username, String password) {
		User user = ((StartService) screen.getService()).login(username, password);
		return user;
	}
}
