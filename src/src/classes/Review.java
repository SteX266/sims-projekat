package classes;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Review {
	protected int id;
	protected int score;
	@JsonIgnore
	private Member member;
	@JsonIgnore
	private Book book;
	public Review() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Review(int id, int score, Member member, Book book) {
		super();
		this.id = id;
		this.score = score;
		this.member = member;
		this.book = book;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Review other = (Review) obj;
		return id == other.id;
	}
	
	
}