package classes;

public enum EmployeeType {
	LIBRARIAN,
	INTERPRETER,
	FULL_STACK,
	ADMINISTRATOR
}
