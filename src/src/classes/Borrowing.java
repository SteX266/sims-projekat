package classes;

import java.time.LocalDate;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Borrowing {
	protected int id;
	protected LocalDate borrowingDate;
	protected LocalDate returnDate;
	@JsonIgnore
	private BookCopy bookCopy;
	@JsonIgnore
	private Member member;
	
	public Borrowing() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Borrowing(int id, LocalDate borrowingDate, LocalDate returnDate, BookCopy bookCopy, Member member) {
		super();
		this.id = id;
		this.borrowingDate = borrowingDate;
		this.returnDate = returnDate;
		this.bookCopy = bookCopy;
		this.member = member;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public LocalDate getBorrowingDate() {
		return borrowingDate;
	}
	public void setBorrowingDate(LocalDate borrowingDate) {
		this.borrowingDate = borrowingDate;
	}
	public LocalDate getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(LocalDate returnDate) {
		this.returnDate = returnDate;
	}
	public BookCopy getBookCopy() {
		return bookCopy;
	}
	public void setBookCopy(BookCopy bookCopy) {
		this.bookCopy = bookCopy;
	}
	
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Borrowing other = (Borrowing) obj;
		return id == other.id;
	}
	
	public Object toCell(int column) {
		switch(column){
			case 0 : return this.borrowingDate;
			case 1 : return this.returnDate;
			case 2 : return this.bookCopy.getBook().getTitle();
			case 3 : return this.bookCopy.getId();
			case 4 : return (this.member.getName()  + " " + member.getSurname());
			default : return null;
		}
	}
}
