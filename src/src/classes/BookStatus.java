package classes;

public enum BookStatus {
	BORROWED,
	SEVERELY_DAMAGED,
	LOST,
	RESERVED,
	AVAILABLE

}
