package classes;

import java.time.LocalDate;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ReservationRequest {
	protected int id;
	protected LocalDate requestDate;
	protected Boolean isApproved;
	protected LocalDate approvalDate;
	@JsonIgnore
	private Book book;
	@JsonIgnore
	private BookCopy bookCopy;
	@JsonIgnore 
	private Member member;
	public ReservationRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ReservationRequest(int id, LocalDate requestDate, Boolean isApproved, LocalDate approvalDate,
			Book book, BookCopy bookCopy, Member member) {
		super();
		this.id = id;
		this.requestDate = requestDate;
		this.isApproved = isApproved;
		this.approvalDate = approvalDate;
		this.book = book;
		this.bookCopy = bookCopy;
		this.member = member;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public LocalDate getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(LocalDate requestDate) {
		this.requestDate = requestDate;
	}
	public Boolean getIsApproved() {
		return isApproved;
	}
	public void setIsApproved(Boolean isApproved) {
		this.isApproved = isApproved;
	}
	public LocalDate getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(LocalDate approvalDate) {
		this.approvalDate = approvalDate;
	}

	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public BookCopy getBookCopy() {
		return bookCopy;
	}
	public void setBookCopy(BookCopy bookCopy) {
		this.bookCopy = bookCopy;
	}
	
	
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
	
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReservationRequest other = (ReservationRequest) obj;
		return id == other.id;
	}
	
	public Object toCell(int column) {
		switch(column){
			case -1 : return this.getId();
			case 0 : return this.book.getTitle();
			case 1 : return this.bookCopy.getId();
			case 2 : return (this.member.getName()  + " " + member.getSurname());
			case 3 : return this.requestDate;
			case 4 : return this.approvalDate;
			case 5: return this.isApproved;
			default : return null;
		}
	}
}