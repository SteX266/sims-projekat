package classes;

public enum MemberType {
	STUDENT,
	EMPLOYED,
	UNEMPLOYED,
	RETIRED,
	PRESCHOOL,
	HONORARY
}
