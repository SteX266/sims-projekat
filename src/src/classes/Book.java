package classes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class Book {
	protected int id;
	protected String ISBN;
	protected String title;
	protected LocalDate publishingDate;
	protected LocalDate printDate;
	protected BookGenre genre;
	@JsonIgnore
	private Publisher publisher;
	@JsonIgnore
	private ArrayList<Author> authors;

	
	public Book() {
		super();
	}

	
	public Book(int id, String iSBN, String title, LocalDate publishingDate, LocalDate printDate, BookGenre genre,
			Publisher publisher, ArrayList<Author> authors) {
		super();
		this.id = id;
		this.ISBN = iSBN;
		this.title = title;
		this.publishingDate = publishingDate;
		this.printDate = printDate;
		this.genre = genre;
		this.publisher = publisher;
		this.authors = authors;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		this.ISBN = iSBN;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public LocalDate getPublishingDate() {
		return publishingDate;
	}

	public void setPublishingDate(LocalDate publishingDate) {
		this.publishingDate = publishingDate;
	}

	public LocalDate getPrintDate() {
		return printDate;
	}

	public void setPrintDate(LocalDate printDate) {
		this.printDate = printDate;
	}

	public BookGenre getGenre() {
		return genre;
	}

	public void setGenre(BookGenre genre) {
		this.genre = genre;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public ArrayList<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(ArrayList<Author> authors) {
		this.authors = authors;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", ISBN=" + ISBN + ", title=" + title + ", publishingDate=" + publishingDate
				+ ", printDate=" + printDate + ", genre=" + genre + ", publisher=" + publisher + ", authors=" + authors + "]";
	}


	@Override
	public int hashCode() {
		return Objects.hash(id);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		return id == other.id;
	}

	public Object toCell(int column) {
		switch(column){
			case 0 : return this.title;
			case 1 : return this.publishingDate;
			case 2 : return this.printDate;
			case 3 : return this.genre;
			case 4 : return this.publisher.getName();
			case 5: return this.authors;
			case 6: return this.ISBN;
			case -1 : return this.id;
			default : return null;
		}
	}	
}
	
	
