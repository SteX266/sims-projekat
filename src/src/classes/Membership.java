package classes;

import java.time.LocalDate;
import java.util.Objects;

public class Membership {
	protected int id;
	protected LocalDate paymentDate;
	protected LocalDate expirationDate;
	public Membership() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Membership(LocalDate paymentDate, LocalDate expirationDate) {
		super();
		this.paymentDate = paymentDate;
		this.expirationDate = expirationDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public LocalDate getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}
	public LocalDate getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(LocalDate expirationDate) {
		this.expirationDate = expirationDate;
	}
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Membership other = (Membership) obj;
		return id == other.id;
	}
	
}
