package classes;

import java.time.LocalDate;

public class Employee extends User {
	protected float salary;
	protected EmployeeType type;
	
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Employee(int id, String name, String surname, String jmbg, LocalDate birthDate, String location,
			String address, String email, String password, Boolean status) {
		super(id, name, surname, jmbg, birthDate, location, address, email, password, status);
		// TODO Auto-generated constructor stub
	}
	
	public Employee(int id, String name, String surname, String jmbg, LocalDate birthDate, String location,
			String address, String email, String password, Boolean status, float salary, EmployeeType type) {
		super(id, name, surname, jmbg, birthDate, location, address, email, password, status);
		this.salary = salary;
		this.type = type;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	public EmployeeType getType() {
		return type;
	}
	public void setType(EmployeeType type) {
		this.type = type;
	}
	
	public Object toCell(int column) {
		switch(column){
			case 0 : return this.name;
			case 1 : return this.surname;
			case 2 : return this.email;
			case 3 : return this.jmbg;
			case 4 : return this.birthDate;
			case 5:	 return this.type;
			case 6:  return this.salary;
			default : return null;
		}
	}
}
	

