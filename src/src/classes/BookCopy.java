package classes;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BookCopy {
	protected int id;
	protected BookStatus status;
	private Book book;
	public BookCopy() {
		super();
	}
	public BookCopy(int id, BookStatus status, Book book) {
		super();
		this.id = id;
		this.status = status;
		this.book = book;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public BookStatus getStatus() {
		return status;
	}
	public void setStatus(BookStatus status) {
		this.status = status;
	}
	@JsonIgnore
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	@Override
	public String toString() {
		return "BookCopy [id=" + id + ", status=" + status + ", book=" + book + "]";
	}
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookCopy other = (BookCopy) obj;
		return id == other.id;
	}
	
	public Object toCell(int column) {
		switch(column){
			case 0 : return this.book.getTitle();
			case 1 : return this.status;
			case 2 : return this.id;
			default : return null;
		}
	}
	

}
