package classes;

import java.time.LocalDate;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Member extends User{
	protected MemberType type;
	@JsonIgnore
	private Membership membership;
	@JsonIgnore
	private ArrayList<Borrowing> borrowings;
	@JsonIgnore
	private ArrayList<ReservationRequest> reservationRequests;
	public Member() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Member(int id, String name, String surname, String jmbg, LocalDate birthDate, String location,
			String address, String email, String password, Boolean status, MemberType type, Membership membership, ArrayList<Borrowing> borrowings,
			ArrayList<ReservationRequest> reservationRequests) {
		super(id, name, surname, jmbg, birthDate, location, address, email, password, status);
		this.type = type;
		this.membership = membership;
		this.borrowings = borrowings;
		this.reservationRequests = reservationRequests;
	}
	public Member(int id, String name, String surname, String jmbg, LocalDate birthDate, String location, String address, String email, String password, MemberType type, Membership membership) {
		super(id, name, surname, jmbg, birthDate, location, address, email, password, true);
		this.type = type;
		this.membership = membership;
		this.borrowings = new ArrayList<Borrowing>();
		this.reservationRequests = new ArrayList<ReservationRequest>();
	}
	
	
	public MemberType getType() {
		return type;
	}
	public void setType(MemberType type) {
		this.type = type;
	}
	public Membership getMembership() {
		return membership;
	}
	public void setMembership(Membership membership) {
		this.membership = membership;
	}
	public ArrayList<Borrowing> getBorrowings() {
		return borrowings;
	}
	public void setBorrowings(ArrayList<Borrowing> borrowings) {
		this.borrowings = borrowings;
	}
	public void addBorrowing(Borrowing b) {
		this.borrowings.add(b);
	}

	public ArrayList<ReservationRequest> getReservationRequests() {
		return reservationRequests;
	}
	public void setReservationRequests(ArrayList<ReservationRequest> reservationRequests) {
		this.reservationRequests = reservationRequests;
	}
	public void addReservationRequest(ReservationRequest rr) {
		this.reservationRequests.add(rr);
	}
	
	public Object toCell(int column) {
		switch(column){
			case 0 : return this.name;
			case 1 : return this.surname;
			case 2 : return this.email;
			case 3 : return this.jmbg;
			case 4 : return this.birthDate;
			case 5: return this.type;
			default : return null;
		}
	}
}
